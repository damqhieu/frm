<?php

use Illuminate\Database\Seeder;

class CustomerTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        $data = [];
        for ($i = 0; $i < 50; $i++) {
            $item = [
                'code' => $faker->swiftBicNumber,
                'name' => 'Customer ' . ($i + 1),
                'numerical_order' => $i,
                'unit' => $faker->company,
                'address' => $faker->address,
                'tax_code' => rand(100000000, 999999999),
                'is_active' => rand(0, 1)
            ];
            $data[] = $item;
        }
        \App\Models\Customer::insert($data);
    }
}
