<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'name' => 'Tong Van Duc',
                'username' => 'ductv',
                'email' => 'admin@frm.com',
                'roles' => 'supper-admin',
                'password' => bcrypt(123456), // secret
                'remember_token' => str_random(10),
                'is_active' => 1,
                'created_at' => date('Y-m-d H:i:s')
            ],
            [
                'name' => 'Manager Customer',
                'username' => 'mc',
                'email' => 'mc@frm.com',
                'roles' => 'manager-customer',
                'password' => bcrypt(123456), // secret
                'remember_token' => str_random(10),
                'is_active' => 1,
                'created_at' => date('Y-m-d H:i:s')
            ],
            [
                'name' => 'Manager Product',
                'username' => 'mp',
                'email' => 'mp@frm.com',
                'roles' => 'manager-product',
                'password' => bcrypt(123456), // secret
                'remember_token' => str_random(10),
                'is_active' => 1,
                'created_at' => date('Y-m-d H:i:s')
            ],
            [
                'name' => 'Manager Order',
                'username' => 'mo',
                'email' => 'mo@frm.com',
                'roles' => 'manager-order',
                'password' => bcrypt(123456), // secret
                'remember_token' => str_random(10),
                'is_active' => 1,
                'created_at' => date('Y-m-d H:i:s')
            ]
        ];

        \App\User::insert($data);
    }
}
