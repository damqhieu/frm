<?php

use Illuminate\Database\Seeder;

class ProductTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        $data = [];
        for ($i = 0; $i < 10; $i++) {
            $item = [
                'code' => $faker->swiftBicNumber,
                'name' => 'Product ' . ($i + 1),
                'image' => 'http://frm.local/admin/dist/img/photo4.jpg',
                'unit' => '',
                'quantity' => rand(1, 10),
                'cost' => 10000000,
                'vat' => 5,
                'price' => 10000000 + (10000000 * 0.05),
                'currency' => 'VND',
                'is_active' => rand(0, 1)
            ];
            $data[] = $item;
        }
        \App\Models\Product::insert($data);
    }
}
