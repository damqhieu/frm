<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInsuranceOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('insurance_orders', function (Blueprint $table) {
            $table->increments('id');
            $table->string('insurance_provider_name')->comment('Tên nhà cung cấp bảo hiểm');
            $table->string('insurance_provider_code')->comment('Mã nhà cung cấp bảo hiểm');
            $table->unsignedInteger('info_company_id')->comment('Thông tin doanh nghiệp');
            $table->integer('total_salary_fund')->comment('Tổng quỹ tiền lương');
            $table->enum('currency', ['VNĐ', 'USD'])->comment('Đơn vị tiền tệ');
            $table->dateTime('registration_date')->comment('Ngày đăng kí');
            $table->longText('regulations')->comment('Các quy định');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('insurance_orders');
    }
}
