<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSaleOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sale_orders', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code');
            $table->string('sales_unit')->comment('don vi ban hang')->nullable();
            $table->string('sales_tax_code')->comment('ma so thue ban hang')->nullable();
            $table->string('sales_address')->comment('dia chi ban hang')->nullable();
            $table->string('sales_phone')->nullable();
            $table->string('sales_account_number')->comment('so tai khoan ban hang')->nullable();
            $table->string('buyer_name')->nullable();
            $table->string('buyer_unit')->comment('don vi mua hang')->nullable();
            $table->string('buyer_tax_code')->comment('ma so thue mua hang')->nullable();
            $table->string('buyer_address')->comment('dia chi mua hang')->nullable();
            $table->string('buyer_phone')->nullable();
            $table->string('buyer_account_number')->nullable();
            $table->string('tax_department')->nullable();
            $table->longText('name_product')->nullable();
            $table->longText('unit')->nullable();
            $table->longText('quantity')->nullable();
            $table->longText('price')->nullable();
            $table->double('vat_tax')->nullable();
            $table->string('status')->nullable()->default('Đã thanh toán');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sale_orders');
    }
}
