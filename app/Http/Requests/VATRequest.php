<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class VATRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'sales_unit' => 'required',
            'sales_tax_code' => 'required',
            'sales_address' => 'required',
            'sales_phone' => 'required',
            'sales_account_number' => 'required|numeric',
            'buyer_name' => 'required',
            'buyer_unit' => 'required',
            'buyer_tax_code' => 'required',
            'buyer_address' => 'required',
            'buyer_phone' => 'required',
            'buyer_account_number' => 'required|numeric',
            'tax_department' => 'required',
            'vat_tax' => 'required',
        ];
    }
}
