<?php

namespace App\Http\Requests;

use App\User;
use Illuminate\Foundation\Http\FormRequest;

class UpdateEmployee extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:191',
            'email' => 'required|string|max:191|email|unique:' . (new User())->getTable() . ',email,' . $this->employee->id,
            'username' => 'required|string|max:191|unique:' . (new User())->getTable() . ',email,' . $this->employee->id,
            'roles' => 'required|string',
            'is_active' => 'nullable|max:2'
        ];
    }
}
