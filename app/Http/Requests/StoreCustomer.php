<?php

namespace App\Http\Requests;

use App\Models\Customer;
use Illuminate\Foundation\Http\FormRequest;

class StoreCustomer extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $tableName = (new Customer())->getTable();
        return [
            'code' => 'required|string|max:191|unique:' . $tableName,
            'name' => 'required|string|max:191|unique:' . $tableName,
            'numerical_order' => 'required|integer|min:1',
            'unit' => 'required|string|max:191',
            'address' => 'required|string|max:191',
            'tax_code' => 'string|max:191',
            'is_active' => 'nullable|max:2'
        ];
    }
}
