<?php

namespace App\Http\Requests;

use App\Models\Product;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StoreProduct extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $tableName = (new Product())->getTable();
        return [
            'code' => 'required|max:191|unique:' . $tableName,
            'name' => 'required|max:191',
            'image' => 'nullable|image',
            'unit' => 'required|max:191|',
            'quantity' => 'required|integer|min:1',
            'cost' => 'required|numeric',
            'vat' => 'required|integer|min:0|max:100',
            'price' => 'required|numeric',
            'currency' => 'required|max:191' . Rule::in(['VND', 'USD', 'EURO']),
            'is_active' => 'nullable|max:2'
        ];
    }
}
