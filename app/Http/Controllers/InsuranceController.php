<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class InsuranceController extends Controller
{
    const VIEW_PATH = 'orders.insurance.';

    public function dashboard()
    {
        $view = self::VIEW_PATH . __FUNCTION__;

        return view($view);
    }

}
