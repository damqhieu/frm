<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreEmployee;
use App\Http\Requests\UpdateEmployee;
use App\User;

class EmployeeController extends Controller
{
    protected $_user;

    protected $_viewFolder;

    public function __construct(User $user)
    {
        $this->_user = $user;
        $this->_viewFolder = 'employees';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = $this->_user->all();
        $view = $this->_viewFolder . '.' . __FUNCTION__;

        return view($view, compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $view = $this->_viewFolder . '.' . __FUNCTION__;

        return view($view);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreEmployee  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreEmployee $request)
    {
        $validated = $request->validated();
        $validated['password'] = bcrypt($request->password);

        if (array_key_exists('is_active', $validated)) {
            $validated['is_active'] = 1;
        } else {
            $validated['is_active'] = 0;
        }

        $flag = $this->_user->create($validated);

        if ($flag) {
            return back()->with('success', 'Tạo mới thành công!');
        }
        return back()->with('warning', 'Tạo mới không thành công!');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  User $employee
     * @return \Illuminate\Http\Response
     */
    public function edit(User $employee)
    {
        $model = $employee;
        $view = $this->_viewFolder . '.' . __FUNCTION__;

        return view($view, compact('model'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateEmployee  $request
     * @param  User $employee
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateEmployee $request, User $employee)
    {
        $validated = $request->validated();

        if (array_key_exists('is_active', $validated)) {
            $validated['is_active'] = 1;
        } else {
            $validated['is_active'] = 0;
        }

        $flag = $employee->update($validated);

        if ($flag) {
            return back()->with('success', 'Cập nhật thành công!');
        }
        return back()->with('warning', 'Cập nhật không thành công!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  User $employee
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $employee)
    {
        $msg = $employee->delete();

        if ($msg) {
            return response()->json(array(
                'status' => 204,
                'msg' => '204: HTTP requests successful'
            ));
        }
        return response()->json(array(
            'status' => 400,
            'msg' => '400: Bad Request'
        ));
    }
}
