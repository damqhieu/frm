<?php

namespace App\Http\Controllers;

use App\Http\Requests\VATRequest;
use App\Models\VATOrder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class VATController extends Controller
{
    public function index()
    {
        $data = VATOrder::all();

        return view('orders.vat.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('orders.vat.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [

        ]);
        if ($validator->fails()) {
            return redirect()
                ->route('vat.create')
                ->withErrors($validator)
                ->withInput();
        }
        // if ($validator->fails()) {
        //     return redirect(route('vat.create'))->with('error','Co loi !!!');
        // }
        $model = new VATOrder();

        $model->code = str_limit( md5(time()),7,'');

        $model->name_product = json_encode($request->name_product);
        $model->unit = json_encode($request->unit);
        $model->quantity = json_encode($request->quantity);
        $model->price = json_encode($request->price);


        $model->fill($request->all());

        $model->save();

        return redirect(route('vat.index'))->with('success','Thành công !!!');
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $model = VATOrder::findOrFail($id);

        return view('orders.vat.edit',compact('model'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            // 'sales_unit' => 'required',
            // 'sales_tax_code' => 'required',
            // 'sales_address' => 'required',
            // 'sales_phone' => 'required',
            // 'sales_account_number' => 'required|numeric',
            // 'buyer_name' => 'required',
            // 'buyer_unit' => 'required',
            // 'buyer_tax_code' => 'required',
            // 'buyer_address' => 'required',
            // 'buyer_phone' => 'required',
            // 'buyer_account_number' => 'required|numeric',
            // 'tax_department' => 'required',
        ]);
          if ($validator->fails()) {
            return redirect()
                ->route('vat.edit',$id)
                ->withErrors($validator)
                ->withInput();
        }
        // if ($validator->fails()) {
        //     return redirect(route('vat.create'))->with('error','Co loi !!!');
        // }

        $model = VATOrder::findOrFail($id);

        // $model->code = str_limit( md5(time()),7,'');

        $model->name_product = json_encode($request->name_product);
        $model->unit = json_encode($request->unit);
        $model->quantity = json_encode($request->quantity);
        $model->price = json_encode($request->price);


        $model->fill($request->all());

        $model->update();

        return redirect(route('vat.index'))->with('success','Thành công !!!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $model = VATOrder::find($id);

        $msg = $model->delete();

        if ($msg) {

            return response()->json(array(
                'status' => 204,
                'msg' => '204: HTTP requests successful'
            ));
        }
        return response()->json(array(
            'status' => 400,
            'msg' => '400: Bad Request'
        ));


    }
}
