<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreProduct;
use App\Http\Requests\UpdateProduct;
use App\Models\Product;

class ProductController extends Controller
{
    protected $_product;

    protected $_tableName;

    public function __construct(Product $product)
    {
        $this->_product = $product;
        $this->_tableName = $this->_product->getTable();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = $this->_product->all();
        $view = $this->_tableName . '.' . __FUNCTION__;

        return view($view, compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $view = $this->_tableName . '.' . __FUNCTION__;

        return view($view);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreProduct $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreProduct $request)
    {
        $validated = $request->validated();
        $validated['code'] = strtoupper($request->code);

        if (array_key_exists('is_active', $validated)) {
            $validated['is_active'] = 1;
        } else {
            $validated['is_active'] = 0;
        }

        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $imageName = time() . $image->getClientOriginalName();
            $image->storeAs('upload/' . $this->_tableName . '/image', $imageName);
            $validated['image'] = 'upload/' . $this->_tableName . '/image/' . $imageName;
        }

        $flag = $this->_product->create($validated);

        if ($flag) {
            return back()->with('success', 'Tạo mới thành công!');
        }
        return back()->with('warning', 'Tạo mới không thành công!');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Product $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        $model = $product;
        $view = $this->_tableName . '.' . __FUNCTION__;

        return view($view, compact('model'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateProduct $request
     * @param  Product $product
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateProduct $request, Product $product)
    {
        if ($request->hasFile('image')) {
            if (file_exists($product->image)) {
                unlink(public_path($product->image));
            }
        }

        $validated = $request->validated();
        $validated['code'] = strtoupper($request->code);

        if (array_key_exists('is_active', $validated)) {
            $validated['is_active'] = 1;
        } else {
            $validated['is_active'] = 0;
        }

        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $imageName = time() . $image->getClientOriginalName();
            $image->storeAs('upload/' . $this->_tableName . '/image', $imageName);
            $validated['image'] = 'upload/' . $this->_tableName . '/image/' . $imageName;
        }

        $flag = $product->update($validated);

        if ($flag) {
            return back()->with('success', 'Cập nhật thành công!');
        }
        return back()->with('warning', 'Cập nhật không thành công!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Product $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        $msg = $product->delete();

        if ($msg) {
            if (file_exists($product->image)) {
                unlink(public_path($product->image));
            }
            return response()->json(array(
                'status' => 204,
                'msg' => '204: HTTP requests successful'
            ));
        }
        return response()->json(array(
            'status' => 400,
            'msg' => '400: Bad Request'
        ));
    }
}
