<?php

namespace App\Http\Controllers;

use App\Models\Order;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    protected $_order;

    protected $_tableName;

    public function __construct(Order $order)
    {
        $this->_order = $order;
        $this->_tableName = $this->_order->getTable();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = $this->_order->with('customer')->get();

        $view = $this->_tableName . '.' . __FUNCTION__;

        return view($view, compact('data'));
    }


    /**
     * Show the form for creating a new resource.
     * @param string $type
     * @return \Illuminate\Http\Response
     */
    public function create($type)
    {
        $view = $this->_tableName . '.' . __FUNCTION__ . '.' . $type;
        return view($view);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param   \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Order $order)
    {
        $model = $order;
        $view = $this->_tableName . '.' . __FUNCTION__;
        return view($view, compact('model'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function print($type)
    {
        $view = $this->_tableName . '.' . __FUNCTION__ . '.' . $type;
        return view($view);
    }
}
