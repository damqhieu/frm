<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreCustomer;
use App\Http\Requests\UpdateCustomer;
use App\Models\Customer;

class CustomerController extends Controller
{
    protected $_customer;

    protected $_tableName;

    public function __construct(Customer $customer)
    {
        $this->_customer = $customer;
        $this->_tableName = $this->_customer->getTable();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = $this->_customer->all();
        $view = $this->_tableName . '.' . __FUNCTION__;

        return view($view, compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $view = $this->_tableName . '.' . __FUNCTION__;

        return view($view);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreCustomer  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreCustomer $request)
    {
        $validated = $request->validated();
        $validated['code'] = strtoupper($request->code);

        if (array_key_exists('is_active', $validated)) {
            $validated['is_active'] = 1;
        } else {
            $validated['is_active'] = 0;
        }

        $flag = $this->_customer->create($validated);

        if ($flag) {
            return back()->with('success', 'Tạo mới thành công!');
        }
        return back()->with('warning', 'Tạo mới không thành công!');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Customer $customer
     * @return \Illuminate\Http\Response
     */
    public function edit(Customer $customer)
    {
        $model = $customer;
        $view = $this->_tableName . '.' . __FUNCTION__;

        return view($view, compact('model'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateCustomer  $request
     * @param  Customer $customer
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateCustomer $request, Customer $customer)
    {
        $validated = $request->validated();
        $validated['code'] = strtoupper($request->code);

        if (array_key_exists('is_active', $validated)) {
            $validated['is_active'] = 1;
        } else {
            $validated['is_active'] = 0;
        }

        $flag = $customer->update($validated);

        if ($flag) {
            return back()->with('success', 'Cập nhật thành công!');
        }
        return back()->with('warning', 'Cập nhật không thành công!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Customer $customer
     * @return \Illuminate\Http\Response
     */
    public function destroy(Customer $customer)
    {
        $msg = $customer->delete();

        if ($msg) {
            return response()->json(array(
                'status' => 204,
                'msg' => '204: HTTP requests successful'
            ));
        }
        return response()->json(array(
            'status' => 400,
            'msg' => '400: Bad Request'
        ));
    }
}
