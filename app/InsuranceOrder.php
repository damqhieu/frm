<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InsuranceOrder extends Model
{
    protected $fillable=[
        'insurance_provider_name',
        'insurance_provider_code',
        'info_company_id',
        'total_salary_fund',
        'registration_date',
        'regulations'

    ];
}
