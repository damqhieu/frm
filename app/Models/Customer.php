<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    protected $fillable = [
        'code',
        'name',
        'numerical_order',
        'unit',
        'address',
        'tax_code',
        'is_active'
    ];
}
