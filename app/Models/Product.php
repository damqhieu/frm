<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    const ACTIVE = 1;
    const INACTIVE = 0;

    public static $currency = ['VND', 'USD', 'EURO'];

    protected $fillable = [
        'code',
        'name',
        'image',
        'unit',
        'quantity',
        'cost',
        'vat',
        'price',
        'currency',
        'is_active'
    ];
}
