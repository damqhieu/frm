<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SaleOrder extends Model
{
    protected $fillable = [
        'sales_unit',
        'sales_tax_code',
        'sales_address',
        'sales_phone',
        'sales_account_number',
        'buyer_name ',
        'buyer_unit',
        'buyer_tax_code',
        'buyer_address',
        'buyer_phone',
        'buyer_account_number',
        'tax_department',
        'name_product',
        'unit',
        'quantity',
        'price',
        'vat_tax',
        'status'
    ];
}
