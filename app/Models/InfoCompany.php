<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class InfoCompany extends Model
{
    protected $fillable = [
        'name_company',
        'name_ceo',
        'address',
        'phone',
        'fax',
        'website',
        'email',
        'field',
        'career',
        'tax_code'
    ];
}
