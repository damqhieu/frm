@extends('layouts.app-admin')

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Danh sách sản phẩm
            {{--<small>advanced tables</small>--}}
        </h1>
        {{--<ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Tables</a></li>
            <li class="active">Data tables</li>
        </ol>--}}
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">

                <div class="box">
                    <div class="box-header">
                        <a href="{{ route('products.create') }}" class="btn btn-success">
                            Thêm mới sản phẩm <i class="fa fa-plus"></i>
                        </a>
                        <a href="{{ route('products.index') }}" class="btn btn-warning">
                            Reload lại trang <i class="fa fa-circle-o"></i>
                        </a>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="datatables" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>STT</th>
                                <th>Ảnh sản phẩm</th>
                                <th>Mã sản phẩm</th>
                                <th>Tên sản phẩm</th>
                                <th>Đơn vị</th>
                                <th>Số lượng</th>
                                <th>Giá cả</th>
                                <th>Giá bán</th>
                                <th>Thuế</th>
                                <th>Loại tiền</th>
                                <th>Trạng thái</th>
                                <th>Hành động</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($data as $item)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td><img src="{{ asset($item->image) }}" width="75px"></td>
                                    <td>{{ $item->code }}</td>
                                    <td>{{ $item->name }}</td>
                                    <td>{{ $item->unit }}</td>
                                    <td>{{ $item->quantity }}</td>
                                    <td>{{ $item->cost }}</td>
                                    <td>{{ $item->price }}</td>
                                    <td>{{ $item->vat }}%</td>
                                    <td>{{ $item->currency }}</td>
                                    <td>
                                        <label class="{{ ($item->is_active === 1) ? 'text-success' : 'text-danger' }}">
                                            {{ ($item->is_active === 1) ? 'Hoạt động' : 'Không hoạt động' }}
                                        </label>
                                    </td>
                                    <td>
                                        <a href="{{ route('products.edit', $item->id) }}"
                                           class="btn btn-xs btn-info"
                                           title="Cập nhật sản phẩm"><i class="fa fa-pencil"></i></a>
                                        <a href="{{ route('products.destroy', $item->id) }}" class="btn btn-xs btn-danger destroy"
                                           title="Xóa sản phẩm"><i class="fa fa-times"></i></a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
@endsection
