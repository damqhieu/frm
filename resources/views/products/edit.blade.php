@extends('layouts.app-admin')

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Cập nhật sản phẩm
            {{--
            <small>advanced tables</small>
            --}}
        </h1>
        {{--
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Tables</a></li>
            <li class="active">Data tables</li>
        </ol>
        --}}
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-info">
                    <div class="box-header">
                        <h3 class="box-title">
                            Thông tin sản phẩm
                        </h3>
                        <!-- tools box -->
                        <div class="pull-right box-tools">
                            <button type="button" class="btn btn-info btn-sm" data-widget="collapse"
                                    data-toggle="tooltip"
                                    title="Collapse">
                                <i class="fa fa-minus"></i></button>
                            <button type="button" class="btn btn-info btn-sm" data-widget="remove" data-toggle="tooltip"
                                    title="Remove">
                                <i class="fa fa-times"></i></button>
                        </div>
                        <!-- /. tools -->
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body pad">
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        <form action="{{ route('products.update', $model->id) }}" method="post"
                              enctype="multipart/form-data">
                            @csrf
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="code">Mã sản phẩm: <span class="text-danger">(*)</span></label>
                                        <input type="text" name="code" value="{{ $model->code }}" class="form-control"
                                               required>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="name">Tên sản phẩm: <span class="text-danger">(*)</span></label>
                                        <input type="text" name="name" value="{{ $model->name }}" class="form-control"
                                               required>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="image">Ảnh sản phẩm: </label>
                                        <input type="file" name="image" class="form-control">
                                        <img src="{{ asset($model->image) }}" width="50px" alt="">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="unit">Đơn vị: <span class="text-danger">(*)</span></label>
                                        <input type="text" name="unit" class="form-control" value="{{ $model->unit }}"
                                               required>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="quantity">Số lượng: <span class="text-danger">(*)</span></label>
                                        <input type="number" name="quantity" class="form-control"
                                               value="{{ $model->quantity }}" required>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="is_active">Trạng thái hoạt động: </label>
                                        <br/>
                                        <input type="checkbox" name="is_active" @if ($model->is_active === 1)
                                        checked
                                               @endif class="minimal">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="cost">Giá cả: <span class="text-danger">(*)</span></label>
                                        <input type="text" name="cost" value="{{ $model->cost }}" class="form-control"
                                               required>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="price">Giá bán: <span class="text-danger">(*)</span></label>
                                        <input type="text" name="price" value="{{ $model->price }}" class="form-control"
                                               required>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="vat">Thuế: </label>
                                        <div class="input-group">
                                            <input type="number" name="vat" value="{{ $model->vat }}"
                                                   class="form-control" required>
                                            <span class="input-group-addon" id="sizing-addon2">%</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="currency">Loại tiền: <span class="text-danger">(*)</span></label>
                                        <select name="currency" class="form-control" required>
                                            @foreach(\App\Models\Product::$currency as $item)
                                                <option @if ($model->currency === $item)
                                                        selected
                                                        @endif value="{{ $item }}">{{ $item }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="text-center" style="padding-bottom: 30px; padding-top: 30px;">
                                    <button type="submit" class="btn btn-info ">Xác nhận cập nhật</button>
                                    <button type="reset" class="btn btn-default ">Định dạng lại</button>
                                    <a href="{{ route('products.index') }}" class="btn btn-warning">Xem danh sách </a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
@endsection

@section('js')
    <script>
        $(function () {
            //iCheck for checkbox and radio inputs
            $('input[type="checkbox"].minimal').iCheck({
                checkboxClass: 'icheckbox_minimal-blue'
            })
        })
    </script>
@endsection
