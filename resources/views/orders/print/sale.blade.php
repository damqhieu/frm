<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Hóa đơn bán hàng</title>
    <style>
        table {
            margin-left: 20%;
            border: 2px solid black;
        }

        .border {
            border-top: 1px solid;
        }

        td {
            border: 1px solid;
        }
        .left{
            float:left;
        }
        .right{
            float:right;
        }

    </style>
    <style type=”text/css” media=”print”>
        #print_button{
            display:none;
        }
        .print_p{
            display:none;
        }
    </style>
</head>
<body>
<table width="60%">
    <tr>
        <td colspan="10">
            <p style="text-align: right"><b>TÊN CỤC THUẾ:................</b></p>
            <p style="text-align: right">Mẫu số: 01GTKT3/001</p>
            <h2 style="text-align: center">HÓA ĐƠN BÁN HÀNG</h2>
            <p class="border">Ký hiệu: 01AA/14P</p>
            <p class="border">Liên 1: Lưu Số: 0000001</p>
            <p class="border">Ngày………tháng………năm 20.....</p>
            <div class="border">
                <p>Đơn vị bán
                    hàng:.............................................................................................................................................</p>
                <p>Mã số
                    thuế:......................................................................................................................................................

                </p>
                <p>Địa
                    chỉ:............................................................................................................................................................

                </p>
                <p>Điện thoại:........................................................Số tài
                    khoản...........................................................................

                </p>
            </div>
            <div class="border">
                <p>Họ tên người mua
                    hàng...................................................................................................................................

                </p>
                <p>Tên đơn
                    vị........................................................................................................................................................

                </p>
                <p>Mã số
                    thuế:......................................................................................................................................................

                </p>
                <p>Địa
                    chỉ..............................................................................................................................................................

                </p>
                <p>Số tài
                    khoản......................................................................................................................................................

                </p>
            </div>
        </td>
    </tr>
    <tr>
        <td>STT</td>
        <td colspan="4">Tên hàng hóa, dịch vụ</td>
        <td>Đơn vị tính</td>
        <td>Số lượng</td>
        <td>Đơn giá</td>
        <td colspan="2">Thành tiền</td>
    </tr>
    <tr>
        <td>1</td>
        <td colspan="4">Tên hàng hóa, dịch vụ 1</td>
        <td>Chiếc</td>
        <td>2</td>
        <td>1.000.000</td>
        <td colspan="2">2.000.000</td>
    </tr>
    <tr>
        <td>2</td>
        <td colspan="4">Tên hàng hóa, dịch vụ 2</td>
        <td>Chiếc</td>
        <td>2</td>
        <td>2.000.000</td>
        <td colspan="2">4.000.000</td>
    </tr>
    <tr>
        <td>3</td>
        <td colspan="4">Tên hàng hóa, dịch vụ 3</td>
        <td>Chiếc</td>
        <td>2</td>
        <td>3.000.000</td>
        <td colspan="2">6.000.000</td>
    </tr>
    <tr>
        <td colspan="10">
            <p class="">Cộng tiền hàng: ...............................

            </p>
            <p class="border">Thuế suất GTGT:...........%, Tiền thuế GTGT: ……………………

            </p>
            <div class="border">
                <p>Tổng cộng tiền thanh toán ............................................................................................................................

                </p>
                <p>Số tiền viết bằng chữ: ....................................................................................................................................

                </p>
            </div>
            <div class="border">
                <div class="left">
                    <p>Người mua hàng</p>
                    <p>(Ký, ghi rõ họ, tên)</p>
                </div>
                <div class="right">
                    <p>Người bán hàng</p>
                    <p>(Ký, đóng dấu, ghi rõ họ, tên)</p>
                </div>
            </div>
            <div style="clear: both; padding-top: 150px"></div>
            <p>(Cần kiểm tra, đối chiếu khi lập, giao, nhận hóa đơn)</p>
        </td>
    </tr>
</table>
<button type="button" style="margin-left: 20%; background: #01ff70; margin-top: 20px; padding: 12px 30px"  onclick="this.style.display='none'; window.print() " class="btn btn-success ">Xuất hóa đơn</button>
<script>
    function In_Content(strid){
        var prtContent = document.getElementById(strid);
        var WinPrint = window.open('','','letf=0,top=0,width=1300,height=800');
        WinPrint.document.write(prtContent.innerHTML);
        WinPrint.document.close();
        WinPrint.focus();
        WinPrint.print();
    }
</script>
</body>
</html>
