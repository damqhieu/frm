<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Hóa đơn lương</title>
    <style>
        table {
            margin-left: 20%;
            border: 2px solid black;
        }

        .border {
            border-top: 1px solid;
        }

        td {
            border: 1px solid;
        }

        .left {
            float: left;
        }

        .right {
            float: right;
        }

    </style>
    <style type=”text/css” media=”print”>
        #print_button {
            display: none;
        }

        .print_p {
            display: none;
        }
    </style>
</head>
<body>
<table width="60%">
    <tr>
        <td colspan="10">
            <p style="text-align: right"><b>TÊN CỤC THUẾ:................</b></p>
            <p style="text-align: right">Mẫu số: 01GTKT3/001</p>
            <h2 style="text-align: center">HÓA ĐƠN LƯƠNG</h2>
            <p class="border">Ký hiệu: 01AA/14P</p>
            <p class="border">Liên 1: Lưu Số: 0000001</p>
            <p class="border">Ngày………tháng………năm 20.....</p>
            <div class="border">
                <p>Họ và
                    tên:.............................................................................................................................................</p>
                <p>Chức vụ</p>
                <p>Điện thoại:........................................................Số tài
                    khoản...........................................................................
                </p>
                <p>Bậc
                    lương:......................................................................................................................................................

                </p>
                <p>Hệ
                    số:............................................................................................................................................................

                </p>
            </div>
        </td>
    </tr>
    <tr>
        <td colspan="4"></td>
        <td>Số sản phẩm(Số công)</td>
        <td>Số tiền</td>
    </tr>
    <tr>
        <td colspan="4">Lương sản phẩm</td>
        <td>5</td>
        <td>5000000</td>
    </tr>
    <tr>
        <td colspan="4">Lương thời gian</td>
        <td>5</td>
        <td>5000000</td>
    </tr>
    <tr>
        <td colspan="4">Nghỉ việc hưởng ...%theo lương</td>
        <td>5</td>
        <td>5000000</td>
    </tr>
    <tr>
        <td colspan="10">
            <div class="border">
                <p>Một số khoản khác : </p>
                <p>Phụ cấp: ..............................................................................................................................................................

                </p>
                <p>Số tiền tạm ứng: ....................................................................................................................................................

                </p>
                <p>BHXH: ..................................................................................................................................................................

                </p>
                <p>Thuế TNCN phải nộp: ..........................................................................................................................................

                </p>
            </div>
        </td>
    </tr>
    <tr>
        <td colspan="10">

            <div class="border">
                <p>Tổng cộng tiền thanh toán
                    ............................................................................................................................

                </p>
                <p>Số tiền viết bằng chữ:
                    ....................................................................................................................................

                </p>
            </div>
            <div class="border">
                <div class="left">
                    <p>Người nhận tiền</p>
                    <p>(Ký, ghi rõ họ, tên)</p>
                </div>
                {{--<div class="right">--}}
                    {{--<p>Người bán hàng</p>--}}
                    {{--<p>(Ký, đóng dấu, ghi rõ họ, tên)</p>--}}
                {{--</div>--}}
            </div>
            <div style="clear: both; padding-top: 150px"></div>
            <p>(Cần kiểm tra, đối chiếu khi lập, giao, nhận hóa đơn)</p>
        </td>
    </tr>
</table>
<button type="button" style="margin-left: 20%; background: #01ff70; margin-top: 20px; padding: 12px 30px"
        onclick="this.style.display='none'; window.print() " class="btn btn-success ">Xuất hóa đơn
</button>
<script>
    function In_Content(strid) {
        var prtContent = document.getElementById(strid);
        var WinPrint = window.open('', '', 'letf=0,top=0,width=1300,height=800');
        WinPrint.document.write(prtContent.innerHTML);
        WinPrint.document.close();
        WinPrint.focus();
        WinPrint.print();
    }
</script>
</body>
</html>
