<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href='https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Material+Icons' rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/vuetify/dist/vuetify.min.css" rel="stylesheet">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, minimal-ui">
    <base href="{{ asset('') }}">

    <link rel="stylesheet" href="admin/bower_components/select2/dist/css/select2.min.css">
    <link rel="stylesheet" href="admin/bower_components/bootstrap/dist/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="admin/bower_components/font-awesome/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="admin/bower_components/Ionicons/css/ionicons.min.css">
    <!-- iCheck for checkboxes and radio inputs -->
    <link rel="stylesheet" href="admin/plugins/iCheck/all.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="admin/dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="admin/dist/css/skins/_all-skins.min.css">
    <!-- Morris chart -->
    <link rel="stylesheet" href="admin/bower_components/morris.js/morris.css">
    <!-- jvectormap -->
    <link rel="stylesheet" href="admin/bower_components/jvectormap/jquery-jvectormap.css">
    <!-- Date Picker -->
    <link rel="stylesheet" href="admin/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
    <!-- Daterange picker -->
    <link rel="stylesheet" href="admin/bower_components/bootstrap-daterangepicker/daterangepicker.css">
    <!-- bootstrap wysihtml5 - text editor -->
    <link rel="stylesheet" href="admin/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
    <link rel="stylesheet" href="admin/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
    <!-- DataTables -->

    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Google Font -->
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">
    <link rel="stylesheet" type="text/css" href="https://printjs-4de6.kxcdn.com/print.min.css">
    <!-- DataTables -->
    <title>Hóa đơn bảo hiểm</title>
    <link rel='stylesheet' type='text/css' href='css/style.css' />
    <link rel="stylesheet" href="css/custom.css">
    <link rel='stylesheet' type='text/css' href='css/print.css' media="print" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
</head>
<body class="skin-blue">
<div class="wrapper " style="height: auto; min-height: 100%;">
  @if (!Auth::guest())
    <div class="wrapper">
        <!-- Main Header -->
        <header class="main-header">

            <!-- Logo -->
            <a href="#" class="logo">
                <b>Finance Resource Management</b>
            </a>

            <!-- Header Navbar -->
            <nav class="navbar navbar-static-top" role="navigation">
                <!-- Sidebar toggle button-->
                <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                    <span class="sr-only">Toggle navigation</span>
                </a>
                <!-- Navbar Right Menu -->
                <div class="navbar-custom-menu">
                    <ul class="nav navbar-nav">
                        <!-- User Account Menu -->
                        <li class="dropdown user user-menu">
                            <!-- Menu Toggle Button -->
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <!-- The user image in the navbar-->
                                <img src="http://infyom.com/images/logo/blue_logo_150x150.jpg"
                                     class="user-image" alt="User Image"/>
                                <!-- hidden-xs hides the username on small devices so only the image appears. -->
                                <span class="hidden-xs">{!! Auth::user()->name !!}</span>
                            </a>
                            <ul class="dropdown-menu">
                                <!-- The user image in the menu -->
                                <li class="user-header">
                                    <img src="http://infyom.com/images/logo/blue_logo_150x150.jpg"
                                         class="img-circle" alt="User Image"/>
                                    <p>
                                        {!! Auth::user()->name !!}
                                        <small>Member since {!! Auth::user()->created_at->format('M. Y') !!}</small>
                                    </p>
                                </li>
                                <!-- Menu Footer-->
                                <li class="user-footer">
                                    <div class="pull-left">
                                        <a href="#" class="btn btn-default btn-flat">Profile</a>
                                    </div>
                                    <div class="pull-right">
                                        <a href="{!! url('/logout') !!}" class="btn btn-default btn-flat"
                                           onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                            Sign out
                                        </a>
                                        <form id="logout-form" action="{{ url('/logout') }}" method="POST"
                                              style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </div>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>

        <!-- Left side column. contains the logo and sidebar -->
        <aside class="main-sidebar" id="sidebar-wrapper">

            <!-- sidebar: style can be found in sidebar.less -->
            <section class="sidebar">

                <!-- Sidebar user panel (optional) -->
                <div class="user-panel">
                    <div class="pull-left image">
                        <img src="http://infyom.com/images/logo/blue_logo_150x150.jpg" class="img-circle"
                             alt="User Image"/>
                    </div>
                    <div class="pull-left info">
                        @if (Auth::guest())
                            <p>InfyOm</p>
                        @else
                            <p>{{ Auth::user()->name}}</p>
                    @endif
                    <!-- Status -->
                        <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                    </div>
                </div>

                <!-- search form (Optional) -->
                <form action="#" method="get" class="sidebar-form">
                    <div class="input-group">
                        <input type="text" name="q" class="form-control" placeholder="Search..."/>
                        <span class="input-group-btn">
            <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i>
            </button>
          </span>
                    </div>
                </form>
                <!-- Sidebar Menu -->

                <ul class="sidebar-menu" data-widget="tree">
                    @include('layouts.menu')
                </ul>
                <!-- /.sidebar-menu -->
            </section>
            <!-- /.sidebar -->
        </aside>
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper" style="min-height: 379px;">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Tạo hóa đơn giá  trị gia tăng
            </h1>

        </section>
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-header">
              {{--               <a href="http://frm.local/frm/orders/vat/create" class="btn btn-primary">Tạo hóa đơn mới <i
                                    class="fa fa-plus"></i></a>
                            <a href="javascript:;" onclick="location.reload();" class="btn btn-warning">Reload lại trang
                                <i
                                    class="fa fa-circle-o"></i></a> --}}

                        <!-- /.box-header -->
                        <div class="box-body">
<div id="app">
                                <form action="{{ route('vat.update',$model->id) }}" method="post">
                                    @csrf
                                    <table width="60%">
                                        <tr>
                                            <td colspan="10">
                                                <v-layout row wrap>
                                                    <v-flex xs2 offset-xs8>
                                                        <p class="" style="margin-top: 2px;text-align: center; font-size: 15px; margin-left: 10px">
                                                            Tên cục thuế : </p>
                                                    </v-flex>
                                                    <v-flex style="max-width: 12%">
                                                        <v-text-field
                                                            name="tax_department" value="{{ $model->tax_department }}"
                                                        ></v-text-field>

                                                    </v-flex>
                                                </v-layout>
                                                <p style="text-align: right">Mẫu số: 01GTKT3/001</p>
                                                <h2 style="text-align: center">HÓA ĐƠN GIÁ TRỊ GIA TĂNG</h2>
                                                <p class="border" style="padding-left: 20px">Ký hiệu: 01AA/14P</p>
                                                <p class="border" style="padding-left: 20px">Liên 1: Lưu Số: 0000001</p>
                                                <p class="border" style="padding-left: 20px">Ngày {{ date('d') }} tháng {{ date('m') }}
                                                    năm {{ date('Y') }}.</p>

                                            </td>
                                        </tr>

                                        <tr>
                                            <td colspan="10">
                                                <div style="margin-top: 12px; width: 100%"></div>
                                                <div class="">
                                                    <v-layout row wrap>
                                                        <v-flex xs3>
                                                            <p class=""
                                                               style=" padding-left: 10px; padding-top: 2px; font-size: 15px; margin-left: 10px">
                                                                Đơn vị bán hàng: </p>
                                                        </v-flex>
                                                        <v-flex xs8>
                                                            <v-text-field
                                                                name="sales_unit" value="{{ $model->sales_unit }}"
                                                            ></v-text-field>

                                                        </v-flex>
                                                    </v-layout>
                                                    <v-layout row wrap>
                                                        <v-flex xs3>
                                                            <p class=""
                                                               style=" padding-left: 10px; padding-top: 2px; font-size: 15px; margin-left: 10px">
                                                                Mã số thuế: </p>
                                                        </v-flex>
                                                        <v-flex xs8>
                                                            <v-text-field
                                                                name="sales_tax_code" value="{{ $model->sales_tax_code }}"
                                                            ></v-text-field>

                                                        </v-flex>
                                                    </v-layout>
                                                    <v-layout row wrap>
                                                        <v-flex xs3>
                                                            <p class=""
                                                               style=" padding-left: 10px; padding-top: 2px; font-size: 15px; margin-left: 10px">
                                                                Địa chỉ: </p>
                                                        </v-flex>
                                                        <v-flex xs8>
                                                            <v-text-field
                                                                name="sales_address" value="{{ $model->sales_address }}"
                                                            ></v-text-field>

                                                        </v-flex>
                                                    </v-layout>
                                                    <v-layout row wrap>
                                                        <v-flex xs3>
                                                            <p class=""
                                                               style=" padding-left: 10px; padding-top: 2px; font-size: 15px; margin-left: 10px">
                                                                Điện thoại: </p>
                                                        </v-flex>
                                                        <v-flex xs3>
                                                            <v-text-field
                                                                name="sales_phone" value="{{ $model->sales_phone }}"
                                                            ></v-text-field>

                                                        </v-flex>
                                                        <v-flex xs3>
                                                            <p class=""
                                                               style=" padding-left: 10px; padding-top: 2px; font-size: 15px; margin-left: 10px">
                                                                Số tài khoản: </p>
                                                        </v-flex>
                                                        <v-flex xs2>
                                                            <v-text-field
                                                                name="sales_account_number" value="{{ $model->sales_account_number }}"
                                                            ></v-text-field>

                                                        </v-flex>
                                                    </v-layout>
                                                </div>
                                                <div style="margin-top: 10px; width: 100%"></div>

                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="10">
                                                <div style="margin-top: 10px; width: 100%"></div>
                                                <div class="">
                                                    <v-layout row wrap>
                                                        <v-flex xs3>
                                                            <p class=""
                                                               style=" padding-left: 10px; padding-top: 2px; font-size: 15px; margin-left: 10px">
                                                               Người mua hàng: </p>
                                                        </v-flex>
                                                        <v-flex xs8>
                                                            <v-text-field
                                                                name="buyer_name" value="{{ $model->buyer_name }}"
                                                            ></v-text-field>

                                                        </v-flex>
                                                    </v-layout>
                                                    <v-layout row wrap>
                                                        <v-flex xs3>
                                                            <p class=""
                                                               style=" padding-left: 10px; padding-top: 2px; font-size: 15px; margin-left: 10px">
                                                                Đơn vị mua hàng: </p>
                                                        </v-flex>
                                                        <v-flex xs8>
                                                            <v-text-field
                                                                name="buyer_unit" value="{{ $model->buyer_unit }}"
                                                            ></v-text-field>

                                                        </v-flex>
                                                    </v-layout>
                                                    <v-layout row wrap>
                                                        <v-flex xs3>
                                                            <p class=""
                                                               style=" padding-left: 10px; padding-top: 2px; font-size: 15px; margin-left: 10px">
                                                                Mã số thuế: </p>
                                                        </v-flex>
                                                        <v-flex xs8>
                                                            <v-text-field
                                                                name="buyer_tax_code" value="{{ $model->buyer_tax_code }}"
                                                            ></v-text-field>

                                                        </v-flex>
                                                    </v-layout>
                                                    <v-layout row wrap>
                                                        <v-flex xs3>
                                                            <p class=""
                                                               style=" padding-left: 10px; padding-top: 2px; font-size: 15px; margin-left: 10px">
                                                                Địa chỉ: </p>
                                                        </v-flex>
                                                        <v-flex xs8>
                                                            <v-text-field
                                                                name="buyer_address" value="{{ $model->buyer_address }}"
                                                            ></v-text-field>

                                                        </v-flex>
                                                    </v-layout>
                                                    <v-layout row wrap>
                                                        <v-flex xs3>
                                                            <p class=""
                                                               style=" padding-left: 10px; padding-top: 2px; font-size: 15px; margin-left: 10px">
                                                                Điện thoại: </p>
                                                        </v-flex>
                                                        <v-flex xs3>
                                                            <v-text-field
                                                                name="buyer_phone" value="{{ $model->buyer_phone }}"
                                                            ></v-text-field>

                                                        </v-flex>
                                                        <v-flex xs3>
                                                            <p class=""
                                                               style=" padding-left: 10px; padding-top: 2px; font-size: 15px; margin-left: 10px">
                                                                Số tài khoản: </p>
                                                        </v-flex>
                                                        <v-flex xs2>
                                                            <v-text-field
                                                                name="buyer_account_number" value="{{ $model->buyer_account_number }}"
                                                            ></v-text-field>

                                                        </v-flex>
                                                    </v-layout>
                                                </div>
                                                <div style="margin-top: 10px; width: 100%"></div>

                                            </td>
                                        </tr>

                                        <tr>
                                          <th colspan="2">Tên mặt hàng</th>
                                          <th colspan="2">Thông tin</th>
                                          <th colspan="2">Đơn giá</th>
                                          <th colspan="2">Số lượng</th>
                                          <th colspan="2">Thành tiền</th>
                                      </tr>

                                        @php
                                            $name_product = json_decode($model->name_product);
                                            $unit = json_decode($model->unit);
                                            $quantity = json_decode($model->quantity);
                                            $price = json_decode($model->price);
                                         $length = sizeof(json_decode($model->name_product));
                                         $total = 0;
                                        @endphp

                                        @for($i = 0; $i<$length; $i++)
                                            <tr class="item-row"></tr>
                                            <tr  class="item-row">

 <td colspan="2" class="item-name"><div class="delete-wpr"><textarea  name="name_product[]">{{ $name_product[$i] }}</textarea><a class="delete" href="javascript:;" title="Remove row">X</a></div></td>


                                              <td colspan="2" class="description"><textarea name="unit[]">{{ $unit[$i] }}</textarea></td>

              <td colspan="2"><textarea class="cost" name="price[]">{{ $price[$i] }}</textarea></td>
              <td colspan="2"><textarea class="qty" name="quantity[]">{{ $quantity[$i] }}</textarea></td>


              <td colspan="2"><span class="price">{{ $quantity[$i]*$price[$i] }}</span></td>

                                                @php  $total += $quantity[$i]*$price[$i]; @endphp
                                            </tr>
                                        @endfor
<tr id="hiderow">
            <td colspan="10"><a id="addrow" href="javascript:;" title="Add a row">Add a row</a></td>
          </tr>
                                        <tr>
                                            <td colspan="10">
                                                <v-layout row wrap>
                                                    <v-flex xs3>
                                                        <p class=""
                                                           style=" paddinhttp://frm.local/frm/orders/vatg-left: 10px; padding-top: 2px; font-size: 15px; margin-left: 10px">
                                                            Cộng tiền hàng: </p>
                                                    </v-flex>
                                                    <v-flex xs8>
                                                        <div id="total">{{ $total  }}</div>
 </v-flex> </v-layout>

                                                <v-layout row wrap class="mb-1">
                                                    <v-flex xs3>
                                                        <p class=""
                                                           style=" padding-left: 10px; padding-top: 2px; font-size: 15px; margin-left: 10px">
                                                            Thuế suất GTGT: </p>
                                                    </v-flex>
                                                    <v-flex xs1>
                                                        <v-text-field name="vat_tax"  id="paid"
                                                                      value="{{ $model->vat_tax }}"
                                                        ></v-text-field>

                                                    </v-flex>
                                                    <v-flex xs1>
                                                        <p class=""
                                                           style=" padding-left: 10px; padding-top: 2px; font-size: 15px; margin-left: 10px">
                                                            % </p>
                                                    </v-flex>
                                                    <v-flex xs3>
                                                        <p class=""
                                                           style=" padding-left: 10px; padding-top: 2px; font-size: 15px; margin-left: 10px">
                                                            Tiền thuế GTGT: </p>
                                                    </v-flex>
                                                    <v-flex xs3>
                                                         <div class="due">{{ $total* ($model->vat_tax/100) }}</div>

                                                    </v-flex>
                                                </v-layout>


                                                <div class="border">
                                                    <v-layout row wrap>
                                                        <v-flex xs4>
                                                            <p class=""
                                                               style=" padding-left: 10px; padding-top: 2px; font-size: 15px; margin-left: 10px">
                                                                Tổng cộng tiền thanh toán: </p>
                                                        </v-flex>
                                                        <v-flex xs7>
                                                            <div id="total2">{{$total + $total* ($model->vat_tax/100) }}</div>
                                                        </v-flex>
                                                    </v-layout>
                                                    <v-layout row wrap class="mb-1">
                                                        <v-flex xs4>
                                                            <p class=""
                                                               style=" padding-left: 10px; padding-top: 2px; font-size: 15px; margin-left: 10px">
                                                                Số tiền viết bằng chữ: </p>
                                                        </v-flex>
                                                        <v-flex xs7>
                                                            <div id="string_total"></div>
                                                        </v-flex>
                                                    </v-layout>
                                                </div>
                                                <div class="border">
                                                    <div class="left">
                                                        <p>Người mua hàng</p>
                                                        <p>(Ký, ghi rõ họ, tên)</p>
                                                    </div>
                                                    <div class="right">
                                                        <p>Người bán hàng</p>
                                                        <p>(Ký, đóng dấu, ghi rõ họ, tên)</p>
                                                    </div>
                                                </div>
                                                <div style="clear: both; padding-top: 50px"></div>
                                                <p>(Cần kiểm tra, đối chiếu khi lập, giao, nhận hóa đơn)</p>
                                            </td>
                                        </tr>
                                    </table>
                                    {{--this.style.display='none';--}}
   <div align="center" style="margin-top:20px;">
                                        <button type="submit"

                                            class="btn btn-primary">Lưu hóa đơn
                                    </button>
                                    <button type="button"

                                            onclick="window.print()" class="btn btn-success ">Xuất hóa đơn
                                    </button>
                                    <button type="reset"

                                            class="btn btn-info">Nhập lại
                                    </button>
                             <a href="javascript:;" onclick="location.reload();" class="btn btn-warning">Reload lại trang
                                <i
                                    class="fa fa-circle-o"></i></a>
                                    </div>
                                </form>

                                <div style="margin-bottom: 40px"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>


        <!-- Main content -->
    </div>

    <!-- Main Footer -->
    <footer class="main-footer" style="max-height: 100px;text-align: center">
        <strong>Copyright © 2016 <a href="#">Dam Quang Hieu</a>.</strong> All rights reserved.
    </footer>

</div>
@else
    <div>
        <!-- Main Header -->

        <h1>Please login!</h1>
        <ul>
            <li class="nav-item">
                <a class="btn btn-success" href="{{ route('login') }}">{{ __('Login') }}</a>
            </li>
            <li class="nav-item">
                <a class="btn btn-success" href="{{ route('register') }}">{{ __('Register') }}</a>
            </li>
        </ul>
    </div>

@endif
    <script type='text/javascript' src='js/jquery-1.3.2.min.js'></script>
    <script type='text/javascript' src='js/example.js'></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="admin/bower_components/jquery-ui/jquery-ui.min.js"></script>
    <script src="admin/dist/js/adminlte.min.js"></script>
    <script src="admin/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/vuetify/dist/vuetify.js"></script>
    <script>
    Vue.component('table-tr', {
        data: function () {
            return {
                count: 0
            }
        },
        template: ""
    });

    new Vue({
        el: '#app',
        data: {
            vat_tax: 0,
            vat_money: 0,
        },
        props: {},
        watch: {
            tinhtien() {
                this.vat_money = total * (this.vat_tax / 100)
            }
        }
    })
</script>
<script src="https://printjs-4de6.kxcdn.com/print.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
<script type="text/javascript">
    $(function () {
        toastr.options = {
            "closeButton": false,
            "debug": false,
            "newestOnTop": false,
            "progressBar": false,
            "positionClass": "toast-top-right",
            "preventDuplicates": false,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        };
        @if(Session::has('errors') )
        // toastr.error('Vui lòng nhập đầy đủ thông tin');
            @php
                $err = '';
                foreach ($errors->all() as $key => $error){
                    $err .=  $error . "<br>";
                }

            @endphp
        //toastr['error']('', '{!! $err!!}');
        toastr.error('Vui lòng nhập đầy đủ thông tin');
        @endif

        $('.alert').delay(2000).slideUp("slow");


        // CKEDITOR.replace('editor'); // ID Tag
    })
</script>

</body>
</html>
