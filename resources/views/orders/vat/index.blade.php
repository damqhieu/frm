@extends('layouts.app-admin')

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Danh sách hóa đơn
            {{--<small>advanced tables</small>--}}
        </h1>
        {{--<ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Tables</a></li>
            <li class="active">Data tables</li>
        </ol>--}}
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <a href="{{ route('vat.create') }}" class="btn btn-primary">Tạo hóa đơn mới <i
                                class="fa fa-plus"></i></a>
                        <a href="javascript:;" onclick="location.reload();" class="btn btn-warning">Reload lại trang <i
                                class="fa fa-circle-o"></i></a>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="datatables" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>STT</th>
                                <th>Mã hóa đơn</th>
                                <th>Tên khách hàng</th>
                                <th>Địa chỉ</th>
                                <th>Ngày tạo</th>
                                <th>Trạng thái</th>
                                <th>Hành động</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($data as $item)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $item->code  }}</td>
                                    <td>{{ $item->buyer_name  }}</td>
                                    <td>{{ $item->buyer_address  }}</td>
                                    <td>{{ $item->created_at->format('d-m-Y')  }}</td>
                                    <td>
                                        <span class="btn btn-xs btn-info">
                                            {{ $item->status  }}
                                        </span>
                                    </td>
                                    <td>
                                        <a href="javascript:;"
                                           class="btn btn-xs btn-success"
                                           title="Xem hóa đơn"><i class="fa fa-eye"></i></a>
                                        <a href="javascript:;"
                                           class="btn btn-xs btn-warning"
                                           title="Xuất hóa đơn"><i class="fa fa-print"></i></a>

                                        <a href="{{ route('vat.edit',$item->id) }}"
                                           class="btn btn-xs btn-info"
                                           title="Cập nhật hóa đơn"><i class="fa fa-pencil"></i></a>
                                        <a href="{{ route('vat.destroy',$item->id) }}"
                                           class="btn btn-xs btn-danger destroy"
                                           title="Xóa hóa đơn"><i class="fa fa-trash"></i></a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
@endsection


