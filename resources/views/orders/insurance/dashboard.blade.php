@extends('layouts.app-admin')

@section('content')
    <section class="content-header">
        <h1>
            {{ $title ?? 'Thống kê bảo hiểm' }}
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">{{ $title ?? 'Thống kê bảo hiểm' }}</li>
        </ol>
    </section>
    <section class="content">
        <!-- Small boxes (Stat box) -->
        <div class="row">
            @foreach(TYPE_INSURANCE as $key => $value)
            <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-aqua">
                    <div class="inner">
                        <h3>1</h3>

                        <p>{{ $value }}</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-android-star-half"></i>
                    </div>
                    <a href="#" class="small-box-footer">Xem chi tiết <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <!-- ./col -->
            @endforeach
        </div>
        <!-- /.row -->
        <!-- Main row -->

        <!-- /.row (main row) -->
    </section>
@endsection
