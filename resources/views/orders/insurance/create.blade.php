<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href='https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Material+Icons' rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/vuetify/dist/vuetify.min.css" rel="stylesheet">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, minimal-ui">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">
    <title>Hóa đơn bảo hiểm</title>
    <style>
        table {
            margin-left: 25%;
            border: 2px solid black;
        }

        .border {
            border-top: 1px solid;
        }

        td {
            border: 1px solid;
        }

        .left {
            float: left;
        }

        .right {
            float: right;
        }

        input[type="text"] {
            padding: 0;
            border-top: 1px solid white;
            border-left: 1px solid white;
            border-right: 1px solid white;
            font-size: 14px;
            color: black;
        }

        .v-input {
            padding: 0;
            margin: 0;
        }

        .v-text-field > .v-input__control > .v-input__slot {
            margin-bottom: 0;
        }

        .v-messages {
            min-height: 0px;
        }

        p {
            padding-top: 0px;
            margin-bottom: 3px;
        }
    </style>
    <style type=”text/css” media=”print”>
        #print_button {
            display: none;
        }

        .print_p {
            display: none;
        }
    </style>
</head>
<body>
<div id="app">
    <form action="{{ route('vat.store') }}" method="post">
        @csrf
        <table width="50%">
            <tr>
                <td colspan="10">
                    <v-layout row wrap>
                        <v-flex xs2 offset-xs8>
                            <p class="" style="margin-top: 2px;text-align: center; font-size: 15px; margin-left: 10px">
                                Tên cục thuế : </p>
                        </v-flex>
                        <v-flex xs2>
                            <v-text-field
                                name="tax_department"
                            ></v-text-field>

                        </v-flex>
                    </v-layout>
                    <p style="text-align: right">Mẫu số: 01GTKT3/001</p>
                    <h2 style="text-align: center">PHIẾU ĐĂNG KÝ <BR>THAM GIA BẢO HIỂM XÃ HỘI</h2>
                    <v-layout row wrap class="mt-3">
                        <v-flex xs5 offset-xs1>
                            <p class=""
                               style="text-align: center; padding-left: 10px; padding-top: 2px; font-size: 15px; margin-left: 10px">
                                Kính gửi: BẢO HIỂM XÃ HỘI </p>
                        </v-flex>
                        <v-flex xs5>
                            <v-text-field
                                name="sales_unit"
                            ></v-text-field>

                        </v-flex>
                    </v-layout>
                    <v-layout row wrap class="mt-4">
                        <v-flex xs3>
                            <p class=""
                               style=" padding-left: 10px; padding-top: 2px; font-size: 15px; margin-left: 10px">
                                Tên doanh nghiệp: </p>
                        </v-flex>
                        <v-flex xs8>
                            <v-text-field
                                name="sales_unit"
                            ></v-text-field>

                        </v-flex>
                    </v-layout>

                    <v-layout row wrap>
                        <v-flex xs3>
                            <p class=""
                               style=" padding-left: 10px; padding-top: 2px; font-size: 15px; margin-left: 10px">
                                Địa chỉ: </p>
                        </v-flex>
                        <v-flex xs3>
                            <v-text-field
                                name="sales_unit"
                            ></v-text-field>

                        </v-flex>
                        <v-flex xs2>
                            <p class=""
                               style=" padding-left: 10px; padding-top: 2px; font-size: 15px; margin-left: 10px"> Điện
                                thoại: </p>
                        </v-flex>
                        <v-flex xs3>
                            <v-text-field
                                name="sales_unit"
                            ></v-text-field>

                        </v-flex>
                    </v-layout>

                    <v-layout row wrap>
                        <v-flex xs4>
                            <p class=""
                               style=" padding-left: 10px; padding-top: 2px; font-size: 15px; margin-left: 10px">
                                Giấy phép hoạt động số: </p>
                        </v-flex>
                        <v-flex xs7>
                            <v-text-field
                                name="sales_unit"
                            ></v-text-field>

                        </v-flex>
                    </v-layout>

                    <v-layout row wrap>
                        <v-flex xs3>
                            <p class=""
                               style=" padding-left: 10px; padding-top: 2px; font-size: 15px; margin-left: 10px">
                                Do: </p>
                        </v-flex>
                        <v-flex xs3>
                            <v-text-field
                                name="sales_unit"
                            ></v-text-field>

                        </v-flex>
                        <v-flex xs2>
                            <p class=""
                               style=" padding-left: 10px; padding-top: 2px; font-size: 15px; margin-left: 10px">
                                Cấp ngày: </p>
                        </v-flex>
                        <v-flex xs3>
                            <v-text-field
                                name="sales_unit"
                            ></v-text-field>

                        </v-flex>
                    </v-layout>

                    <v-layout row wrap>
                        <v-flex xs3>
                            <p class=""
                               style=" padding-left: 10px; padding-top: 2px; font-size: 15px; margin-left: 10px">
                                Ngành nghề hoạt động: </p>
                        </v-flex>
                        <v-flex xs8>
                            <v-text-field
                                name="sales_unit"
                            ></v-text-field>

                        </v-flex>
                    </v-layout>

                    <v-layout row wrap>
                        <v-flex xs3>
                            <p class=""
                               style=" padding-left: 10px; padding-top: 2px; font-size: 15px; margin-left: 10px">
                                Số tài khoản: </p>
                        </v-flex>
                        <v-flex xs3>
                            <v-text-field
                                name="sales_unit"
                            ></v-text-field>

                        </v-flex>
                        <v-flex xs2>
                            <p class=""
                               style=" padding-left: 10px; padding-top: 2px; font-size: 15px; margin-left: 10px">
                                Tại: </p>
                        </v-flex>
                        <v-flex xs3>
                            <v-text-field
                                name="sales_unit"
                            ></v-text-field>

                        </v-flex>
                    </v-layout>

                    <v-layout row wrap>
                        <v-flex xs3>
                            <p class=""
                               style=" padding-left: 10px; padding-top: 2px; font-size: 15px; margin-left: 10px">
                                Đại diện doanh nghiệp: </p>
                        </v-flex>
                        <v-flex xs8>
                            <v-text-field
                                name="sales_unit"
                            ></v-text-field>

                        </v-flex>
                    </v-layout>

                    <v-layout row wrap>
                        <v-flex xs3>
                            <p class=""
                               style=" padding-left: 10px; padding-top: 2px; font-size: 15px; margin-left: 10px">
                                Số chứng minh thư nhân dân: </p>
                        </v-flex>
                        <v-flex xs8>
                            <v-text-field
                                name="sales_unit"
                            ></v-text-field>

                        </v-flex>
                    </v-layout>

                    <v-layout row wrap>
                        <v-flex xs3>
                            <p class=""
                               style=" padding-left: 10px; padding-top: 2px; font-size: 15px; margin-left: 10px">
                                Ngày cấp: </p>
                        </v-flex>
                        <v-flex xs3>
                            <v-text-field
                                name="sales_unit"
                            ></v-text-field>

                        </v-flex>
                        <v-flex xs2>
                            <p class=""
                               style=" padding-left: 10px; padding-top: 2px; font-size: 15px; margin-left: 10px">
                                Nơi cấp: </p>
                        </v-flex>
                        <v-flex xs3>
                            <v-text-field
                                name="sales_unit"
                            ></v-text-field>

                        </v-flex>
                    </v-layout>

                    <v-layout row wrap>
                        <v-flex xs12 class="mt-3">

                            <p class=""
                               style=" padding-left: 10px; padding-top: 2px; font-size: 15px; margin-left: 10px">
                                <b>Đăng ký nộp BHXH cho người lao động trong doanh nghiệp như sau: </b></p>
                        </v-flex>
                        <v-flex xs4>
                            <p class=""
                               style=" padding-left: 10px; padding-top: 2px; font-size: 15px; margin-left: 10px">
                                Tổng số người lao động: </p>
                        </v-flex>
                        <v-flex xs1>
                            <v-text-field
                                name="sales_unit"
                            ></v-text-field>
                        </v-flex>

                    </v-layout>

                    <v-layout row wrap>
                        <v-flex xs4>
                            <p class=""
                               style=" padding-left: 10px; padding-top: 2px; font-size: 15px; margin-left: 10px">
                                Tổng quỹ tiền lương tháng: </p>
                        </v-flex>
                        <v-flex xs3>
                            <v-text-field
                                name="sales_unit"
                            ></v-text-field>

                        </v-flex>

                    </v-layout>
                    <v-layout row wrap>
                        <v-flex xs12 class="ml-5">

                            <p class=""
                               style=" padding-left: 10px; padding-top: 2px; font-size: 15px; margin-left: 10px">
                                (Kèm bộ mẫu danh sách lao động theo quy định do BHXH cung cấp )</p>
                        </v-flex>
                        <v-flex xs12 class="mt-3">

                            <p class=""
                               style=" padding-left: 10px; padding-top: 2px; font-size: 15px; margin-left: 10px">
                                <b>Đăng ký thực hiện chế độ BHXH theo đúng quy định: </b></p>
                        </v-flex>

                        <v-flex xs12>

                            <p class=""
                               style=" padding-left: 10px; padding-top: 2px; font-size: 15px; margin-left: 10px">
                                -Trích nộp BHXH đầy đủ cho người lao động làm việc tại đơn vị, chuyển nộp kịp thời tiền BHXH
                            </p>
                            <p class=""
                               style=" padding-left: 10px; padding-top: 2px; font-size: 15px; margin-left: 10px">
                                -BHYT hàng tháng về cơ quan BHXH
                            </p>
                            <p class=""
                               style=" padding-left: 10px; padding-top: 2px; font-size: 15px; margin-left: 10px">
                                -Hàng tháng lập danh sách lao động và quỹ tiền lương điều chỉnh mức nộp BHXH khi có thay đổi gửi về cho cơ quan
                                BHXH , kịp thời cấp phiếu KCB cho người lao động
                            </p>
                            <p class=""
                               style=" padding-left: 10px; padding-top: 2px; font-size: 15px; margin-left: 10px">
                                -Thực hiện các quy định về cấp sổ BHXH và lập thủ tục để giải quyết kịp thời chế độ danh sách BHXH cho người lao động
                            </p>
                            <p class=""
                               style=" padding-left: 10px; padding-top: 2px; font-size: 15px; margin-left: 10px">
                                Đơn vị cam kết đảm bảo thực hiện đúng các quy định của luật lao động và điều lệ BHXH
                            </p>
                        </v-flex>
                    </v-layout>
                   <v-layout row wrap>
                        <v-flex xs6 class="mb-5"  style="margin-top: 24px !important; text-align: center" >
                            <b>XÁC NHẬN CỦA BHXH</b>
                            <p>GIÁM ĐỐC</p>
                        </v-flex>
                       <v-flex xs6 class="mb-5"  style="margin-top: 10px; text-align: center" >
                           <i style="float:right">Ngày...tháng....năm....</i><BR>
                           <b>ĐẠI DIỆN DOANH NGHIỆP</b>
                           <p>GIÁM ĐỐC</p>
                       </v-flex>
                   </v-layout>
                </td>

            </tr>

        </table>

        {{--this.style.display='none';--}}
        <button type="submit"
                style="margin-left: 25%;margin-top: 20px;background-color: rgb(205, 215, 216);padding: 8px;"
                class="btn btn-primary">Lưu hóa đơn
        </button>
        <button type="button"
                style="margin-left: 1%;background-color: rgb(205, 215, 216); padding: 8px; margin-top: 20px; "
                onclick=" window.print() " class="btn btn-success ">Xuất hóa đơn
        </button>
        <button type="reset"
                style="margin-left: 1%;margin-top: 20px;background-color: rgb(205, 215, 216);padding: 8px 12px;"
                class="btn btn-primary">Nhập lại
        </button>
        <a style="text-decoration:none;margin-left: 1%;margin-top: 20px;background-color: rgb(205, 215, 216);padding: 8px 12px;"
           href="{{ route('vat.index') }}">
            Quay lại
        </a>

    </form>
    <div style="margin-bottom: 40px"></div>
</div>


<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
<script src="https://cdn.jsdelivr.net/npm/vuetify/dist/vuetify.js"></script>
<script>
    Vue.component('table-tr', {
        data: function () {
            return {
                count: 0
            }
        },
        template: ""
    });

    new Vue({
        el: '#app',
        data: {
            vat_tax: 0,
            vat_money: 0,
        },
        props: {},
        watch: {
            tinhtien() {
                this.vat_money = total * (this.vat_tax / 100)
            }
        }


    })
</script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
<script type="text/javascript">
    $(function () {
        toastr.options = {
            "closeButton": false,
            "debug": false,
            "newestOnTop": false,
            "progressBar": false,
            "positionClass": "toast-top-right",
            "preventDuplicates": false,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        };
        @if(Session::has('error') )
        toastr.error('Vui lòng nhập đầy đủ thông tin');
        @endif

        $('.alert').delay(2000).slideUp("slow");


        // CKEDITOR.replace('editor'); // ID Tag
    })
</script>
<script>
    function In_Content(strid) {
        var prtContent = document.getElementById(strid);
        var WinPrint = window.open('', '', 'letf=0,top=0,width=1300,height=800');
        WinPrint.document.write(prtContent.innerHTML);
        WinPrint.document.close();
        WinPrint.focus();
        WinPrint.print();
    }
</script>
</body>
</html>
