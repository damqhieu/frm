@extends('layouts.app-admin')

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Danh sách hóa đơn
            {{--<small>advanced tables</small>--}}
        </h1>
        {{--<ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Tables</a></li>
            <li class="active">Data tables</li>
        </ol>--}}
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <a href="{{ route('orders.create', 'gtgt') }}" class="btn btn-success">Tạo hóa đơn GTGT</a>
                        <a href="{{ route('orders.create', 'sale') }}" class="btn btn-info">Tạo hóa đơn Bán hàng</a>
                        <a href="{{ route('orders.create', 'baohiem') }}" class="btn btn-default">Tạo hóa đơn Bảo
                            hiểm</a>
                        <a href="{{ route('orders.create', 'salary') }}" class="btn btn-soundcloud">Tạo hóa đơn
                            Lương</a>
                        <a href="{{ route('orders.index', '') }}" class="btn btn-warning">Reload lại trang <i
                                    class="fa fa-circle-o"></i></a>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="datatables" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>STT</th>
                                <th>Mã hóa đơn</th>
                                <th>Tên khách hàng</th>
                                <th>Địa chỉ</th>
                                <th>Chi phí</th>
                                <th>Loại hóa đơn</th>
                                <th>Ngày tạo</th>
                                <th>Trạng thái</th>
                                <th>Hành động</th>
                            </tr>
                            </thead>
                            <tbody>
                            {{--@foreach($data as $item)--}}
                            {{--<tr>--}}
                            {{--<td>{{ $loop->iteration }}</td>--}}
                            {{--<td>{{ $item->code }}</td>--}}
                            {{--<td>{{ $item->customer->name }}</td>--}}
                            {{--<td>{{ $item->customer->address }}</td>--}}
                            {{--<td>{{ $item->total_after_tax }}</td>--}}
                            {{--<td>{{ $item->created_at }}</td>--}}
                            {{--<td>--}}
                            {{--<label class="text-success">--}}
                            {{--Đã thanh toán--}}
                            {{--</label>--}}
                            {{--</td>--}}
                            {{--<td>--}}
                            {{--<a href=""--}}
                            {{--class="btn btn-xs btn-success"--}}
                            {{--title="Xuất hóa đơn"><i class="fa fa-circle-o"></i></a>--}}
                            {{--<a href="{{ route('orders.edit', $item->id) }}"--}}
                            {{--class="btn btn-xs btn-info"--}}
                            {{--title="Cập nhật hóa đơn"><i class="fa fa-pencil"></i></a>--}}
                            {{--<a href="{{ route('orders.destroy', $item->id) }}" class="btn btn-xs btn-danger destroy"--}}
                            {{--title="Xóa hóa đơn"><i class="fa fa-times"></i></a>--}}
                            {{--</td>--}}
                            {{--</tr>--}}
                            {{--@endforeach--}}
                            @for($i = 1; $i <= 15 ; $i++)
                                <tr>
                                    <td>{{ $i }}</td>
                                    <td>Mã hóa đơn {{ $i }}</td>
                                    <td>khách hàng {{ $i }}</td>
                                    <td>Địa chỉ {{ $i }}</td>
                                    <td>{{ $i * 1000000 }}</td>
                                    <td>Loại hóa đơn {{ $i }}</td>
                                    <td>Địa chỉ {{ $i }}</td>
                                    <td>{{ date('d-m-Y') }}</td>
                                    <td><span class="{{ array_random(['text-info', 'text-danger', 'text-green']) }}">{{ array_random(['Pendding', 'Done', 'Fail']) }}</span></td>
                                    <td>
                                        <a href="javascript:;"
                                           class="btn btn-xs btn-success"
                                           title="Xuất hóa đơn"><i class="fa fa-circle-o"></i></a>
                                        <a href="javascript:;"
                                           class="btn btn-xs btn-info"
                                           title="Cập nhật hóa đơn"><i class="fa fa-pencil"></i></a>
                                        <a href="javascript:;"
                                           class="btn btn-xs btn-danger destroy"
                                           title="Xóa hóa đơn"><i class="fa fa-times"></i></a>
                                    </td>
                                </tr>
                            @endfor
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
@endsection
