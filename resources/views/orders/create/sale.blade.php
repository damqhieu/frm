@extends('layouts.app-admin')

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Thêm mới hóa đơn bán hàng
            {{--
            <small>advanced tables</small>
            --}}
        </h1>
        {{--
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Tables</a></li>
            <li class="active">Data tables</li>
        </ol>
        --}}
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-info">
                    <div class="box-header">
                        <h3 class="box-title">
                            Thông tin hóa đơn
                        </h3>
                        <!-- tools box -->
                        <div class="pull-right box-tools">
                            <button type="button" class="btn btn-info btn-sm" data-widget="collapse"
                                    data-toggle="tooltip"
                                    title="Collapse">
                                <i class="fa fa-minus"></i></button>
                            <button type="button" class="btn btn-info btn-sm" data-widget="remove" data-toggle="tooltip"
                                    title="Remove">
                                <i class="fa fa-times"></i></button>
                        </div>
                        <!-- /. tools -->
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body pad">
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        <form action="" method="post" id="Content_ID" enctype="multipart/form-data">
                            @csrf
                            <a href="javascript:;" class="text-green"
                               style="display: block;text-decoration: underline; font-weight: bold;">
                                Bên bán hàng :</a>
                            <br/>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="code">Đơn vị bán hàng: <span class="text-danger">(*)</span></label>
                                        <input type="text" name="code" class="form-control" required>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="name">Mã số thuế: <span class="text-danger">(*)</span></label>
                                        <input type="text" name="name" class="form-control" required>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="image">Địa chỉ: <span
                                                    class="text-danger">(*)</span></label>
                                        <input type="text" name="name" class="form-control" required>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="unit">Điện thoại: <span class="text-danger">(*)</span></label>
                                        <input type="text" name="unit" class="form-control" required>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="unit">Số tài khoản: <span class="text-danger">(*)</span></label>
                                        <input type="text" name="unit" class="form-control" required>
                                    </div>
                                </div>
                            </div>
                            <a href="javascript:;" class="text-green"
                               style="display: block;text-decoration: underline; font-weight: bold;">
                                Bên mua hàng :</a>
                            <br/>
                            <div class="row">

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="quantity">Họ và tên người mua hàng: <span
                                                    class="text-danger">(*)</span></label>
                                        <input type="number" name="quantity" class="form-control" required>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="is_active">Tên đơn vị: <span
                                                    class="text-danger">(*)</span></label>
                                        <input type="number" name="quantity" class="form-control" required>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="cost">Mã số thuế: <span class="text-danger">(*)</span></label>
                                        <input type="text" name="cost" class="form-control" required>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="price">Hình thức thanh toán: <span
                                                    class="text-danger">(*)</span></label>
                                        <input type="text" name="price" class="form-control" required>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="price">Số tài khoản: <span
                                                    class="text-danger">(*)</span></label>
                                        <input type="text" name="price" class="form-control" required>
                                    </div>
                                </div>
                                <div class="box-body">
                                    <table id="datatables" class="table table-bordered table-striped">
                                        <tr>
                                            <th>STT</th>
                                            <th>Tên hàng hóa, dịch vụ</th>
                                            <th>Đơn vị tính</th>
                                            <th>Số lượng</th>
                                            <th>Đơn giá</th>
                                            <th>Thành tiền</th>
                                        </tr>
                                        <tr>
                                            <td>1</td>
                                            <td>Hàng hóa 1</td>
                                            <td>Chiếc</td>
                                            <td>3</td>
                                            <td>400.000</td>
                                            <td>1.200.000</td>
                                        </tr>
                                        <tr>
                                            <td>2</td>
                                            <td>Hàng hóa 2</td>
                                            <td>Chiếc</td>
                                            <td>3</td>
                                            <td>400.000</td>
                                            <td>1.200.000</td>
                                        </tr>
                                        <tr>
                                            <td>3</td>
                                            <td>Hàng hóa 3</td>
                                            <td>Chiếc</td>
                                            <td>3</td>
                                            <td>400.000</td>
                                            <td>1.200.000</td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">Tổng tiền :</td>
                                            <td colspan="4">3.600.000</td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">Thuế xuất giá trị gia tăng 10%:</td>
                                            <td colspan="4">360.000</td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">Tổng tiền thanh toán :</td>
                                            <td colspan="4">3.960.000</td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">Số tiền bằng chữ :</td>
                                            <td colspan="4">Ba triệu chín trăm sáu mươi nghìn đồng</td>
                                        </tr>
                                    </table>


                                </div>

                            </div>
                            <div class="row">
                                <div class="text-center" style="padding-bottom: 30px; padding-top: 30px;">
                                    <a href="{{ route('orders.print', 'sale') }}" class="btn btn-success">Xem hóa
                                        đơn</a>
                                    <button type="submit" class="btn btn-info ">Xác nhận tạo mới</button>
                                    <button type="reset" class="btn btn-default ">Định dạng lại</button>
                                    <a href="{{ route('products.index') }}" class="btn btn-warning">Xem danh sách </a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
@endsection

@section('js')

    <script>
        $(function () {
            //iCheck for checkbox and radio inputs
            $('input[type="checkbox"].minimal').iCheck({
                checkboxClass: 'icheckbox_minimal-blue'
            })
        })
    </script>

@endsection

@section('css')
    <style>
        table, tr, td, th {
            text-align: center;
        }
    </style>
@endsection
