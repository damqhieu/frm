@extends('layouts.app-admin')

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Thêm mới hóa đơn bảo hiểm thân thể
            {{--
            <small>advanced tables</small>
            --}}
        </h1>
        {{--
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Tables</a></li>
            <li class="active">Data tables</li>
        </ol>
        --}}
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-info">
                    <div class="box-header">
                        <h3 class="box-title">
                            Thông tin hóa đơn
                        </h3>
                        <!-- tools box -->
                        <div class="pull-right box-tools">
                            <button type="button" class="btn btn-info btn-sm" data-widget="collapse"
                                    data-toggle="tooltip"
                                    title="Collapse">
                                <i class="fa fa-minus"></i></button>
                            <button type="button" class="btn btn-info btn-sm" data-widget="remove" data-toggle="tooltip"
                                    title="Remove">
                                <i class="fa fa-times"></i></button>
                        </div>
                        <!-- /. tools -->
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body pad">
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        <form action="" method="post" id="Content_ID" enctype="multipart/form-data">
                            @csrf
                            <a href="javascript:;" class="text-green"
                               style="display: block;text-decoration: underline; font-weight: bold;">
                                Thông tin :</a>
                            <br/>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="code">Họ tên: <span class="text-danger">(*)</span></label>
                                        <input type="text" name="code" class="form-control" required>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="name">Ngày sinh: <span class="text-danger">(*)</span></label>
                                        <input type="text" name="name" class="form-control" required>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="name">Địa chỉ: <span class="text-danger">(*)</span></label>
                                        <input type="text" name="name" class="form-control" required>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="image">Chức vụ: <span
                                                    class="text-danger">(*)</span></label>
                                        <input type="text" name="name" class="form-control" required>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="unit">Phòng ban: <span class="text-danger">(*)</span></label>
                                        <input type="text" name="unit" class="form-control" required>
                                    </div>
                                </div>

                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="unit">Nội dung: <span class="text-danger">(*)</span></label>
                                        <textarea name="" class="editor form-control" id="editor" cols="30"
                                                  rows="10"></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="text-center" style="padding-bottom: 30px; padding-top: 30px;">
                                    <a href="{{ route('orders.print', 'baohiem') }}" class="btn btn-success">Xem hóa
                                        đơn</a>
                                    <button type="submit" class="btn btn-info ">Xác nhận tạo mới</button>
                                    <button type="reset" class="btn btn-default ">Định dạng lại</button>
                                    <a href="{{ route('products.index') }}" class="btn btn-warning">Xem danh sách </a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
@endsection

@section('js')
    <script>
        $(function () {
            CKEDITOR.replace('editor'); // ID Tag
        })
    </script>
@endsection

@section('css')
    <style>
        table, tr, td, th {
            text-align: center;
        }
    </style>
@endsection
