@extends('layouts.app-admin')

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Danh sách nhân viên
            {{--<small>advanced tables</small>--}}
        </h1>
        {{--<ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Tables</a></li>
            <li class="active">Data tables</li>
        </ol>--}}
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">

                <div class="box">
                    <div class="box-header">
                        <a href="{{ route('employees.create') }}" class="btn btn-success">
                            Thêm mới nhân viên <i class="fa fa-plus"></i>
                        </a>
                        <a href="{{ route('employees.index') }}" class="btn btn-warning">
                            Reload lại trang <i class="fa fa-circle-o"></i>
                        </a>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="datatables" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>STT</th>
                                <th>Tên nhân viên</th>
                                <th>Tên đăng nhập</th>
                                <th>Email</th>
                                <th>Quyền</th>
                                <th>Trạng thái</th>
                                <th>Hành động</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($data as $item)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $item->name }}</td>
                                    <td>{{ $item->username }}</td>
                                    <td>{{ $item->email }}</td>
                                    <td>{{ $item->roles }}</td>
                                    <td>
                                        <label class="{{ ($item->is_active === 1) ? 'text-success' : 'text-danger' }}">
                                            {{ ($item->is_active === 1) ? 'Hoạt động' : 'Không hoạt động' }}
                                        </label>
                                    </td>
                                    <td>
                                        <a href="{{ route('employees.roles', $item->id) }}"
                                           class="btn btn-xs btn-info"
                                           title="Phân quyền nhân viên"><i class="fa fa-percent"></i></a>
                                        <a href="{{ route('employees.edit', $item->id) }}"
                                           class="btn btn-xs btn-info"
                                           title="Cập nhật nhân viên"><i class="fa fa-pencil"></i></a>
                                        <a href="{{ route('employees.destroy', $item->id) }}" class="btn btn-xs btn-danger destroy"
                                           title="Xóa nhân viên"><i class="fa fa-times"></i></a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
@endsection
