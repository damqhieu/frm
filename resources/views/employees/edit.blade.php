@extends('layouts.app-admin')

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Cập nhật nhân viên
            {{--
            <small>advanced tables</small>
            --}}
        </h1>
        {{--
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Tables</a></li>
            <li class="active">Data tables</li>
        </ol>
        --}}
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-info">
                    <div class="box-header">
                        <h3 class="box-title">
                            Thông tin nhân viên
                        </h3>
                        <!-- tools box -->
                        <div class="pull-right box-tools">
                            <button type="button" class="btn btn-info btn-sm" data-widget="collapse"
                                    data-toggle="tooltip"
                                    title="Collapse">
                                <i class="fa fa-minus"></i></button>
                            <button type="button" class="btn btn-info btn-sm" data-widget="remove" data-toggle="tooltip"
                                    title="Remove">
                                <i class="fa fa-times"></i></button>
                        </div>
                        <!-- /. tools -->
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body pad">
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        <form action="{{ route('employees.update', $model->id) }}" method="post">
                            @csrf
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="name">Tên nhân viên: <span class="text-danger">(*)</span></label>
                                        <input type="text" name="name" value="{{ $model->name }}" class="form-control" required>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="username">Tên đăng nhập: <span class="text-danger">(*)</span></label>
                                        <input type="text" name="username" value="{{ $model->username }}" class="form-control" required>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="roles">Chọn quyền: <span class="text-danger">(*)</span></label>
                                        <select name="roles" required class="form-control">
                                            <option @if ($model->roles === 'supper-admin') selected @endif value="supper-admin">supper-admin</option>
                                            <option @if ($model->roles === 'manager-customer') selected @endif value="manager-customer">manager-customer</option>
                                            <option @if ($model->roles === 'manager-product') selected @endif value="manager-product">manager-product</option>
                                            <option @if ($model->roles === 'manager-order') selected @endif value="manager-order">manager-order</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="email">Email: <span class="text-danger">(*)</span></label>
                                        <input type="email" name="email" value="{{ $model->email }}" class="form-control" required>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="is_active">Trạng thái hoạt động: </label>
                                        <br/>
                                        <input type="checkbox" @if ($mode->is_active === 1) checked @endif name="is_active" class="minimal">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="text-center" style="padding-bottom: 30px; padding-top: 30px;">
                                    <button type="submit" class="btn btn-info ">Xác nhận cập nhật</button>
                                    <button type="reset" class="btn btn-default ">Định dạng lại</button>
                                    <a href="{{ route('employees.index') }}" class="btn btn-warning">Xem danh sách </a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
@endsection

@section('js')
    <script>
        $(function () {
            //iCheck for checkbox and radio inputs
            $('input[type="checkbox"].minimal').iCheck({
                checkboxClass: 'icheckbox_minimal-blue'
            })
        })
    </script>
@endsection
