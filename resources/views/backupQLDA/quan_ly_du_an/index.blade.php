@extends('layouts.app-admin')

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Danh sách dự án
            {{--<small>advanced tables</small>--}}
        </h1>
        {{--<ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Tables</a></li>
            <li class="active">Data tables</li>
        </ol>--}}
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">

                <div class="box">
                    <div class="box-header">
                        <a href="{{ route('quan-ly-du-an.lapMoiDuAn') }}">
                            <button class="btn btn-primary">Thêm mới dự án <i class="fa fa-plus"></i></button>
                        </a>

                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="datatables" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>STT</th>
                                <th>Mã dự án</th>
                                <th>Tên dự án</th>
                                <th>Thời gian</th>
                                <th>Trưởng dụ án</th>
                                <th>Trạng thái</th>
                                <th>Hành động</th>
                            </tr>
                            </thead>
                            <tbody>
                            @for ($i = 0; $i < 10; $i++)
                                <tr>
                                    <td>{{ $i +1}}</td>
                                    <td>{{ str_limit(strtoupper(md5(random_bytes(10))), 10, '') }}</td>
                                    <td>{{ 'Tên dự án  ' . $i }}</td>
                                    <td>{{ date('d/m/Y ',time()) . '---' . date('d/m/Y ',time()) }}</td>
                                    <td>{{ 'Trưởng dự án ' . $i  }}</td>
                                    <td>
                                        @if(rand(1,10)%2)
                                            <button class="btn btn-success">{{ 'Đã duyệt ' }}</button>
                                        @elseif(rand(1,10)%3)
                                            <button class="btn btn-warning">{{ 'Đang triển khai ' }}</button>
                                            @else
                                            <button class="btn btn-basic ">{{ 'Đã đóng  ' }}</button>
                                        @endif
                                    </td>

                                    <td>
                                        <a href="{{ route('quan-ly-du-an.xemChiTietDuAn') }}"
                                           class="btn btn-xs btn-info"
                                           title="Xem chi tiết dự án "><i class="fa fa-eye"></i></a>
                                        <a href="javascript:;" class="btn btn-xs btn-danger"
                                           title="Cập nhật thông tin dự án "><i class="fa fa-pencil"></i></a>
                                    </td>
                                </tr>
                            @endfor
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
@endsection
