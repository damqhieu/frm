@extends('layouts.app-admin')

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Lập mới dự án
            {{--<small>advanced tables</small>--}}
        </h1>
        {{--<ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Tables</a></li>
            <li class="active">Data tables</li>
        </ol>--}}
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">

                <div class="box box-info">
                    <div class="box-header">
                        <h3 class="box-title">
                            Thông tin cơ bản
                        </h3>
                        <!-- tools box -->

                        <div class="pull-right box-tools">
                            <button type="button" class="btn btn-info btn-sm" data-widget="collapse"
                                    data-toggle="tooltip"
                                    title="Collapse">
                                <i class="fa fa-minus"></i></button>
                            <button type="button" class="btn btn-info btn-sm" data-widget="remove" data-toggle="tooltip"
                                    title="Remove">
                                <i class="fa fa-times"></i></button>
                        </div>
                        <!-- /. tools -->
                    </div>
                    <!-- /.box-header -->
                    <div  class="box-body pad" >
                        <form action="">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="code">Mã Dự Án:</label>
                                    <input type="text" class="form-control">
                                </div>
                                <!-- Date range -->
                                <!-- /.form group -->
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="code">Trưởng dự án:</label>
                                    <select class="form-control select2" style="width: 100%;">
                                        <option>Trưởng dự án </option>
                                        <option>Trưởng dự án 1 </option>
                                        <option>Trưởng dự án 2 </option>
                                        <option>Trưởng dự án 3 </option>
                                        <option>Trưởng dự án 4  </option>
                                        <option>Trưởng dự án 5</option>

                                    </select>
                                </div>
                                <!-- checkbox -->

                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="code">Tên Dự Án:</label>
                                    <input type="text" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Thời gian: </label>
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </div>
                                        <input type="text" class="form-control pull-right" id="reservation">
                                    </div>
                                    <!-- /.input group -->
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="code">Trạng Thái:</label>
                                    <select class="form-control select2" style="width: 100%;">
                                        <option>Ý tưởng </option>
                                        <option >Chuẩn bị </option>
                                        <option >Kế hoạch </option>
                                        <option>Đang triển khai</option>
                                        <option>Đang hoàn thành </option>
                                        <option>Hoàn thành </option>
                                        <option>Đang đánh giá</option>
                                        <option>Đã đóng </option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="editor1">Nội dung chi tiết dự án</label>
                                    <textarea id="editor1" rows="3" class="form-control"></textarea>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="box box-info  collapsed-box">
                    <div class="box-header">
                        <h3 class="box-title">
                            Thiết lập
                        </h3>
                        <!-- tools box -->

                        <div class="pull-right box-tools">
                            <button type="button" class="btn btn-info btn-sm" data-widget="collapse"
                                    data-toggle="tooltip"
                                    title="Collapse">
                                <i class="fa fa-plus"></i></button>
                            <button type="button" class="btn btn-info btn-sm" data-widget="remove" data-toggle="tooltip"
                                    title="Remove">
                                <i class="fa fa-times"></i></button>
                        </div>
                        <!-- /. tools -->
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body pad" style="display: none">
                        <form action="">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="code">Chủ quản :</label>
                                    <select class="form-control select2" style="width: 100%;">
                                        <option> Tập đoàn kinh tế  </option>
                                        <option >Công ty cổ phẩn  </option>
                                        <option>A company </option>
                                        <option>Công ty thương mại </option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="code">Nhóm dự án :</label>
                                    <select class="form-control select2" style="width: 100%;">
                                        <option> Dự án quan trọng  </option>
                                        <option >Dự án số 1 </option>
                                        <option>Dự án nhóm A </option>
                                        <option>Dự án nhóm B </option>
                                    </select>
                                </div>
                                <!-- Date range -->
                                <div class="form-group">
                                    <label for="code">Hồ sơ: </label>
                                    <select class="form-control select2" style="width: 100%;">
                                        <option> Hồ sơ  dự án nội bộ   </option>
                                        <option> Hồ sơ  dự án nội bộ và có đối tác bên ngoài   </option>
                                        <option >Hồ sơ dự án phát triển kinh doanh </option>
                                        <option >Hồ sơ dự án phát triển đầu tư  </option>
                                        <option>Hồ sơ dự án Marketing </option>
                                    </select>
                                </div>
                                <!-- /.form group -->
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="code">Giám đốc</label>
                                    <select class="form-control select2" style="width: 100%;">
                                        <option> Giám đốc 1 </option>
                                        <option> Giám đốc 2 </option>
                                        <option> Giám đốc 3 </option>
                                        <option> Giám đốc 4 </option>
                                        <option> Giám đốc 5 </option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="code">Mục tiêu</label>
                                    <select class="form-control select2" style="width: 100%;">
                                        <option>Triển khai dự án phần mềm</option>
                                        <option>Triển khai dự án xây dựng</option>
                                        <option>Marketing giới thiệu sản phẩm </option>
                                        <option>Triển khai ứng dụng phần mềm </option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="code">Nghiệp vụ</label>
                                    <select class="form-control select2" style="width: 100%;">
                                        <option>Địa chất xây dựng</option>
                                        <option>Kế toán</option>
                                        <option>Hành chính </option>
                                        <option>Kinh doanh  </option>
                                        <option>Bất động sản  </option>
                                        <option>Công nghệ thông tin </option>
                                        <option>Marketing</option>
                                        <option>Quản trị nhân lực</option>
                                        <option>Kinh tế lao động </option>
                                    </select>
                                </div>
                                <!-- checkbox -->

                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="code">Đối tác </label>
                                    <input type="text" class="form-control" placeholder="Lựa chọn ">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="box box-info  collapsed-box">
                    <div class="box-header">
                        <h3 class="box-title">
                            Cơ cấu
                        </h3>
                        <!-- tools box -->

                        <div class="pull-right box-tools">
                            <button type="button" class="btn btn-info btn-sm" data-widget="collapse"
                                    data-toggle="tooltip"
                                    title="Collapse">
                                <i class="fa fa-plus"></i></button>
                            <button type="button" class="btn btn-info btn-sm" data-widget="remove" data-toggle="tooltip"
                                    title="Remove">
                                <i class="fa fa-times"></i></button>
                        </div>
                        <!-- /. tools -->
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body pad" style="display: none">
                        <form action="">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="code">Bảo trợ dự án  :</label>
                                    <select class="form-control select2" multiple="multiple" style="width: 100%;">
                                        <option> ASC Basic 1  </option>
                                        <option> ASC Basic 2  </option>
                                        <option> ASC Basic 3  </option>
                                        <option >Quản trị hệ thống 1 </option>
                                        <option >Quản trị hệ thống 2 </option>
                                        <option >Quản trị hệ thống 3 </option>
                                        <option>A company </option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="code">Trưởng ban chỉ đạo :</label>
                                    <select class="form-control select2" style="width: 100%;">
                                        <option> Trưởng ban chỉ đạo  </option>
                                        <option> Trưởng ban chỉ đạo 1 </option>
                                        <option> Trưởng ban chỉ đạo 2 </option>
                                        <option> Trưởng ban chỉ đạo 3 </option>
                                        <option> Trưởng ban chỉ đạo 4 </option>

                                    </select>
                                </div>
                                <!-- Date range -->
                                <div class="form-group">
                                    <label for="code">Thành viên ban chỉ đạo : </label>
                                    <select class="form-control select2" multiple="multiple" style="width: 100%;">
                                        <option> Thành viên ban chỉ đạo    </option>
                                        <option> Thành viên ban chỉ đạo 1   </option>
                                        <option> Thành viên ban chỉ đạo 2   </option>
                                        <option> Thành viên ban chỉ đạo 3   </option>
                                        <option> Thành viên ban chỉ đạo 4   </option>
                                        <option> Thành viên ban chỉ đạo 5  </option>
                                        <option> Thành viên ban chỉ đạo 6  </option>
                                        <option> Thành viên ban chỉ đạo 7   </option>
                                        <option> Thành viên ban chỉ đạo 8  </option>

                                    </select>
                                </div>
                                <!-- /.form group -->
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="code">Giám đốc dự án :</label>
                                    <select class="form-control select2" style="width: 100%;">
                                        <option>Giám đốc dự án</option>
                                        <option>Giám đốc dự án 1</option>
                                        <option>Giám đốc dự án 2</option>
                                        <option>Giám đốc dự án 3</option>
                                        <option>Giám đốc dự án 4 </option>
                                        <option>Giám đốc dự án 5</option>

                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="code">Điều phối viên :</label>
                                    <select class="form-control select2" multiple="multiple" style="width: 100%;">
                                        <option>TĐiều phối viên  </option>
                                        <option>TĐiều phối viên 1 </option>
                                        <option>TĐiều phối viên 2 </option>
                                        <option>TĐiều phối viên 3 </option>
                                        <option>TĐiều phối viên 4 </option>
                                        <option>TĐiều phối viên 5 </option>

                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="code">Trợ lý: </label>
                                    <select class="form-control select2" multiple="multiple" style="width: 100%;">
                                        <option>Trợ lý  </option>
                                        <option>Trợ lý 1 </option>
                                        <option>Trợ lý 2 </option>
                                        <option>Trợ lý 3 </option>
                                        <option>Trợ lý 4 </option>
                                        <option>Trợ lý 5 </option>
                                        <option>Trợ lý 6 </option>

                                    </select>
                                </div>
                                <!-- checkbox -->

                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="code">Thành viên : </label>
                                    <select class="form-control select2" multiple="multiple" style="width: 100%;">
                                        <option>Thành viên   </option>
                                        <option>Thành viên  1 </option>
                                        <option>Thành viên  2 </option>
                                        <option>Thành viên  3 </option>
                                        <option>Thành viên  4 </option>
                                        <option>Thành viên  5 </option>
                                        <option>Thành viên  6 </option>

                                    </select>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="code">Người liên quan  : </label>
                                    <select class="form-control select2" multiple="multiple" style="width: 100%;">
                                        <option>Người liên quan    </option>
                                        <option>Người liên quan   1 </option>
                                        <option>Người liên quan   2 </option>
                                        <option>Người liên quan   3 </option>
                                        <option>Người liên quan   4 </option>
                                        <option>Người liên quan   5 </option>
                                        <option>Người liên quan   6 </option>
                                    </select>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="box box-info  collapsed-box">
                    <div class="box-header">
                        <h3 class="box-title">
                           Tài chính
                        </h3>
                        <!-- tools box -->

                        <div class="pull-right box-tools">
                            <button type="button" class="btn btn-info btn-sm" data-widget="collapse"
                                    data-toggle="tooltip"
                                    title="Collapse">
                                <i class="fa fa-plus"></i></button>
                            <button type="button" class="btn btn-info btn-sm" data-widget="remove" data-toggle="tooltip"
                                    title="Remove">
                                <i class="fa fa-times"></i></button>
                        </div>
                        <!-- /. tools -->
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body pad" style="display: none">
                        <form action="">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="code">Loại ngân sách  :</label>
                                    <select class="form-control select2" style="width: 100%;">
                                        <option> Trong ngân sách   </option>
                                        <option>Ngoài ngân sách  </option>
                                        <option> Trong và ngoài ngân sách  </option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="code">Loại tiền :</label>
                                    <select class="form-control select2" style="width: 100%;">
                                        <option> VND   </option>
                                        <option> USD </option>
                                        <option> EUR </option>
                                        <option> JP </option>
                                        <option>GBP </option>

                                    </select>
                                </div>
                                <!-- Date range -->
                                <div class="form-group">
                                    <label for="code">Ngân sách đầu năm :</label>
                                    <input type="number " class="form-control">
                                </div>
                                <div class="form-group">
                                    <label for="code">Ngân sách đề xuất   :</label>
                                    <input type="number " class="form-control">
                                </div>
                                <div class="form-group">
                                    <label for="code">Ngân sách kế hoạch  :</label>
                                    <input type="number " class="form-control">
                                </div>
                            </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="code">Chi phí thực hiện  :</label>
                                        <input type="number " class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <label for="code">Còn phải chi :</label>
                                        <input type="number " class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <label for="code">Tổng tiền hợp đồng  :</label>
                                        <input type="number " class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <label for="code">Tổng chi hợp đồng  :</label>
                                        <input type="number " class="form-control">
                                    </div>
                                </div>
                                <!-- /.form group -->

                            <div class="col-md-6">
                                <!-- checkbox -->

                            </div>

                        </form>
                    </div>
                </div>
                <div class="box box-info  collapsed-box">
                    <div class="box-header">
                        <h3 class="box-title">
                           Mô tả
                        </h3>
                        <!-- tools box -->

                        <div class="pull-right box-tools">
                            <button type="button" class="btn btn-info btn-sm" data-widget="collapse"
                                    data-toggle="tooltip"
                                    title="Collapse">
                                <i class="fa fa-plus"></i></button>
                            <button type="button" class="btn btn-info btn-sm" data-widget="remove" data-toggle="tooltip"
                                    title="Remove">
                                <i class="fa fa-times"></i></button>
                        </div>
                        <!-- /. tools -->
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body pad" style="display: none">
                        <form action="">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="editor2">Phạm vi </label>
                                    <textarea id="editor2" rows="3" class="form-control"></textarea>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="editor3">Mục tiêu </label>
                                    <textarea id="editor3" rows="3" class="form-control"></textarea>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="editor4">Địa điểm </label>
                                    <textarea id="editor4" rows="3" class="form-control"></textarea>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
                <div class="box box-info  collapsed-box">
                    <div class="box-header">
                        <h3 class="box-title">
                           Nhiều hơn
                        </h3>
                        <!-- tools box -->

                        <div class="pull-right box-tools">
                            <button type="button" class="btn btn-info btn-sm" data-widget="collapse"
                                    data-toggle="tooltip"
                                    title="Collapse">
                                <i class="fa fa-plus"></i></button>
                            <button type="button" class="btn btn-info btn-sm" data-widget="remove" data-toggle="tooltip"
                                    title="Remove">
                                <i class="fa fa-times"></i></button>
                        </div>
                        <!-- /. tools -->
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body pad" style="display: none">
                        <form action="">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="editor5">Ghi chú  </label>
                                    <textarea id="editor5" rows="3" class="form-control"></textarea>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="editor6">Ghi nhớ của tôi  </label>
                                    <textarea id="editor6" rows="3" class="form-control"></textarea>
                                </div>
                            </div>


                        </form>
                    </div>
                    <div class="text-center" style="padding-bottom: 30px">
                        <button type="submit " class="btn btn-info ">Submit</button>
                        <button type="reset " class="btn btn-danger ">Cancel</button>
                    </div>
                </div>

                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
@endsection

@section('css')
    <!-- date-range-picker -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-daterangepicker/2.1.27/daterangepicker.min.css">
    <!-- Select2 -->

    <!-- bootstrap datepicker -->
    <link rel="stylesheet" href="admin/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">

@endsection
@section('js')
    <!-- CK Editor -->
    <script src="admin/bower_components/ckeditor/ckeditor.js"></script>
    <!-- date-range-picker -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-daterangepicker/3.0.3/moment.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-daterangepicker/2.1.27/daterangepicker.min.js"></script>
    <!-- Select2 -->
    <script src="admin/bower_components/select2/dist/js/select2.full.min.js"></script>
    <script type="text/javascript">
        $(function () {

            $('#reservation').daterangepicker();
            //Date picker
            $('#datepicker').datepicker({
                autoclose: true
            });
            CKEDITOR.replace('editor1');
            CKEDITOR.replace('editor2');
            CKEDITOR.replace('editor3');
            CKEDITOR.replace('editor4');
            CKEDITOR.replace('editor5');
            CKEDITOR.replace('editor6');
        })
    </script>

@endsection
