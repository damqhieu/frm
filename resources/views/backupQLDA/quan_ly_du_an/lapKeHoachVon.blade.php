@extends('layouts.app-admin')

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Lập kế hoạch vốn
            {{--<small>advanced tables</small>--}}
        </h1>
        {{--<ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Tables</a></li>
            <li class="active">Data tables</li>
        </ol>--}}
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">

                <div class="box box-info">
                    <div class="box-header">
                        <h3 class="box-title">
                            Lập kế hoạch vốn
                        </h3>
                        <!-- tools box -->

                        <div class="pull-right box-tools">
                            <button type="button" class="btn btn-info btn-sm" data-widget="collapse"
                                    data-toggle="tooltip"
                                    title="Collapse">
                                <i class="fa fa-minus"></i></button>
                            <button type="button" class="btn btn-info btn-sm" data-widget="remove" data-toggle="tooltip"
                                    title="Remove">
                                <i class="fa fa-times"></i></button>
                        </div>
                        <!-- /. tools -->
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body pad">
                        <form action="">

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="code">Danh sách dự án:</label>
                                    <select class="form-control select2" style="width: 100%;">
                                        <option selected="selected"> ---- Chọn dự án ----</option>
                                        @for($i = 1; $i < 10; $i++)
                                            <option> Dự án {{ $i }}</option>
                                        @endfor
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="code">Giai đoạn dự án:</label>
                                    <select class="form-control select2" style="width: 100%;">
                                        <option selected="selected"> ---- Chọn giai đoạn ----</option>
                                        @for($i = 1; $i < 10; $i++)
                                            <option> Giai đoạn {{ $i }}</option>
                                        @endfor
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="code">Số tiền cho giai đoạn : </label>
                                    <input type="text" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="code">Người phụ trách trực tiếp : </label>
                                    <input type="text" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Thời gian: </label>
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </div>
                                        <input type="text" class="form-control pull-right" id="reservation">
                                    </div>
                                    <!-- /.input group -->
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="code">Trạng Thái:</label>
                                    <select class="form-control select2" style="width: 100%;">
                                        <option selected="selected"></option>
                                        <option>Ý tưởng</option>
                                        <option>Chuẩn bị</option>
                                        <option>Kế hoạch</option>
                                        <option>Đang triển khai</option>
                                        <option>Đang hoàn thành</option>
                                        <option>Hoàn thành</option>
                                        <option>Đang đánh giá</option>
                                        <option>Đã đóng</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="editor1">Nội dung chi tiết:</label>
                                    <textarea id="editor" rows="3" class="form-control"></textarea>
                                </div>
                            </div>
                            <div class="text-center" style="padding-bottom: 30px">
                                <button type="submit " class="btn btn-info ">Submit</button>
                                <button type="reset " class="btn btn-danger ">Cancel</button>
                            </div>
                        </form>
                    </div>
                </div>

                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
@endsection

@section('css')
    <!-- date-range-picker -->
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-daterangepicker/2.1.27/daterangepicker.min.css">
    <!-- Select2 -->

    <!-- bootstrap datepicker -->
    <link rel="stylesheet" href="admin/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">

@endsection
@section('js')
    <!-- CK Editor -->
    <script src="admin/bower_components/ckeditor/ckeditor.js"></script>
    <!-- date-range-picker -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-daterangepicker/3.0.3/moment.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-daterangepicker/2.1.27/daterangepicker.min.js"></script>
    <!-- Select2 -->
    <script src="admin/bower_components/select2/dist/js/select2.full.min.js"></script>
    <script type="text/javascript">
        $(function () {

            $('#reservation').daterangepicker();
            //Date picker
            $('#datepicker').datepicker({
                autoclose: true
            })
        })
    </script>

@endsection
