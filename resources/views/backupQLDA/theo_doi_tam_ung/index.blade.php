@extends('layouts.app-admin')

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Theo dõi tạm ứng
            {{--<small>advanced tables</small>--}}
        </h1>
        {{--<ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Tables</a></li>
            <li class="active">Data tables</li>
        </ol>--}}
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">

                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Danh sách tạm ứng </h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="datatables" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>Mã tạm ứng</th>
                                <th>Tên nhân viên</th>
                                <th>Số tiền tạm ứng </th>
                                <th>Trưởng dụ án</th>
                                <th>Trạng thái</th>
                                <th>Hành động</th>
                            </tr>
                            </thead>
                            <tbody>
                            @for ($i = 0; $i < 10; $i++)
                                <tr>
                                    <td>{{ str_limit(strtoupper(md5(random_bytes(10))), 10, '') }}</td>
                                    <td>{{ 'Tên dự án  ' . $i }}</td>
                                    <td>{{ date('d/m/Y ',time()) . '---' . date('d/m/Y ',time()) }}</td>
                                    <td>{{ 'Trưởng dự án ' . $i  }}</td>
                                    <td>{{ 'Đã duyệt  ' }}</td>

                                    <td>
                                        <a href="{{ route('quan-ly-du-an.danhSachCongViec') }}" class="btn btn-xs btn-info"
                                           title="Xem chi tiết dự án "><i class="fa fa-eye"></i></a>
                                        <a href="javascript:;" class="btn btn-xs btn-success"
                                           title="Cập nhật thông tin dự án "><i class="fa fa-pencil"></i></a>
                                    </td>
                                </tr>
                            @endfor
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
@endsection
