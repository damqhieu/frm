<li class="header">MAIN NAVIGATION</li>
<li><a href="{{ route('dashboard') }}">
        <i class="fa fa-dashboard"></i> Dashboard
    </a></li>
<li class=" treeview">
    <a href="javascript:;">
        <i class="fa fa-dashboard"></i> <span>Quản lý nhân viên</span>
        <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
    </a>
    <ul class="treeview-menu">
        <li><a href="{{ route('employees.create') }}"><i class="fa fa-plus"></i>Tạo mới nhân viên</a></li>
        <li><a href="{{ route('employees.index') }}"><i class="fa fa-circle-o"></i>Danh sách nhân viên</a></li>
    </ul>
</li>
<li class=" treeview">
    <a href="javascript:;">
        <i class="fa fa-dashboard"></i> <span>Quản lý khách hàng</span>
        <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
    </a>
    <ul class="treeview-menu">
        <li><a href="{{ route('customers.create') }}"><i class="fa fa-plus"></i>Tạo mới khách hàng</a></li>
        <li><a href="{{ route('customers.index') }}"><i class="fa fa-circle-o"></i>Danh sách khách hàng</a></li>
    </ul>
</li>
<li class=" treeview">
    <a href="javascript:;">
        <i class="fa fa-dashboard"></i> <span>Quản lý sản phẩm</span>
        <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
    </a>
    <ul class="treeview-menu">
        <li><a href="{{ route('products.create') }}"><i class="fa fa-plus"></i>Tạo mới sản phẩm</a></li>
        <li><a href="{{ route('products.index') }}"><i class="fa fa-circle-o"></i>Danh sách sản phẩm</a></li>
    </ul>
</li>
<li class=" treeview">
    <a href="javascript:;">
        <i class="fa fa-dashboard"></i> <span>Quản lý hóa đơn</span>
        <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
        </span>
    </a>
    <ul class="treeview-menu">
        <li><a href="{{ route('vat.index') }}"><i class="fa fa-plus"></i>Hóa đơn GTGT</a></li>
        <li><a href="{{ route('insurances.dashboard') }}"><i class="fa fa-plus"></i>Hóa đơn Bảo hiểm</a></li>
    </ul>
</li>
