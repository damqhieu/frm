@extends('layouts.app-admin')

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Danh sách khách hàng
            {{--<small>advanced tables</small>--}}
        </h1>
        {{--<ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Tables</a></li>
            <li class="active">Data tables</li>
        </ol>--}}
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">

                <div class="box">
                    <div class="box-header">
                        <a href="{{ route('customers.create') }}" class="btn btn-success">
                            Thêm mới khách hàng <i class="fa fa-plus"></i>
                        </a>
                        <a href="{{ route('customers.index') }}" class="btn btn-warning">
                            Reload lại trang <i class="fa fa-circle-o"></i>
                        </a>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="datatables" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>STT</th>
                                <th>Mã khách hàng</th>
                                <th>Tên khách hàng</th>
                                <th>Số thứ tự khách hàng</th>
                                <th>Thuộc về đơn vị</th>
                                <th>Địa chỉ</th>
                                <th>Mã số thuế</th>
                                <th>Trạng thái</th>
                                <th>Hành động</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($data as $item)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $item->code }}</td>
                                    <td>{{ $item->name }}</td>
                                    <td>{{ $item->numerical_order }}</td>
                                    <td>{{ $item->unit }}</td>
                                    <td>{{ $item->address }}</td>
                                    <td>{{ $item->tax_code }}</td>
                                    <td>
                                        <label class="{{ ($item->is_active === 1) ? 'text-success' : 'text-danger' }}">
                                            {{ ($item->is_active === 1) ? 'Hoạt động' : 'Không hoạt động' }}
                                        </label>
                                    </td>
                                    <td>
                                        <a href="{{ route('customers.edit', $item->id) }}"
                                           class="btn btn-xs btn-info"
                                           title="Cập nhật khách hàng"><i class="fa fa-pencil"></i></a>
                                        <a href="{{ route('customers.destroy', $item->id) }}" class="btn btn-xs btn-danger destroy"
                                           title="Xóa khách hàng"><i class="fa fa-times"></i></a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
@endsection
