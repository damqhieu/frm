<?php

define('TYPE_INSURANCE', [
    'CTDKBH' => 'Công ty đăng ký bảo hiểm',
    'NVDKBH' => 'Nhân viên đăng ký bảo hiểm',
    'NV-CT' => 'Nhân viên đóng bảo hiểm với Công ty',
    'CT-TCBH' => 'Công ty đóng bảo hiểm với tổ chức bảo hiểm',
]);
