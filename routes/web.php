<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('frm/dashboard', 'HomeController@index')->name('dashboard');

Route::group(['prefix' => 'frm', 'middlewre' => 'auth'], function () {
    Route::group(['prefix' => 'products'], function () {
        Route::get('/', 'ProductController@index')->name('products.index');
        Route::get('create', 'ProductController@create')->name('products.create');
        Route::post('store', 'ProductController@store')->name('products.store');
        Route::get('edit/{product}', 'ProductController@edit')->name('products.edit');
        Route::post('update/{product}', 'ProductController@update')->name('products.update');
        Route::post('destroy/{product}', 'ProductController@destroy')->name('products.destroy');
    });
    Route::group(['prefix' => 'orders'], function () {
        Route::get('print/{order}', 'OrderController@print')->name('orders.print');

        // route vat (gia tri gia tang) hieudq
        Route::group(['prefix' => 'vat'], function () {
            Route::get('/', 'VATController@index')->name('vat.index');
            Route::get('create', 'VATController@create')->name('vat.create');
            Route::post('store', 'VATController@store')->name('vat.store');
            Route::get('edit/{id}', 'VATController@edit')->name('vat.edit');
            Route::post('update/{id}', 'VATController@update')->name('vat.update');
            Route::post('destroy/{id}', 'VATController@destroy')->name('vat.destroy');
        });

        // route vat (gia tri gia tang) hieudq
        Route::group(['prefix' => 'insurances'], function () {
            Route::get('dashboard', 'InsuranceController@dashboard')->name('insurances.dashboard');
             Route::get('company-register-insurances', function(){
                return view('orders.insurance.create');
             });
        });

        Route::group(['prefix' => 'sale'], function () {
            Route::get('/', 'SaleController@index')->name('sales.index');
            Route::get('create', 'SaleController@create')->name('sales.create');
            Route::post('store', 'SaleController@store')->name('sales.store');
            Route::get('edit/{id}', 'SaleController@edit')->name('sales.edit');
            Route::post('update/{id}', 'SaleController@update')->name('sales.update');
            Route::post('destroy/{id}', 'SaleController@destroy')->name('sales.destroy');
        });
    });
    Route::group(['prefix' => 'employees'], function () {
        Route::get('/', 'EmployeeController@index')->name('employees.index');
        Route::get('create', 'EmployeeController@create')->name('employees.create');
        Route::post('store', 'EmployeeController@store')->name('employees.store');
        Route::get('edit/{employee}', 'EmployeeController@edit')->name('employees.edit');
        Route::post('update/{employee}', 'EmployeeController@update')->name('employees.update');
        Route::post('destroy/{employee}', 'EmployeeController@destroy')->name('employees.destroy');
        Route::match(['GET', 'POST'], 'roles/{employee}', 'EmployeeController@roles')->name('employees.roles');
    });
    Route::group(['prefix' => 'customers'], function () {
        Route::get('/', 'CustomerController@index')->name('customers.index');
        Route::get('create', 'CustomerController@create')->name('customers.create');
        Route::post('store', 'CustomerController@store')->name('customers.store');
        Route::get('edit/{customer}', 'CustomerController@edit')->name('customers.edit');
        Route::post('update/{customer}', 'CustomerController@update')->name('customers.update');
        Route::post('destroy/{customer}', 'CustomerController@destroy')->name('customers.destroy');
    });

});
